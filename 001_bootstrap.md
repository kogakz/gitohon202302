# bootstrap

## ツリー構造

```text
├── src
│   ├── main
│   │   └── java
│   │       └── datadog
│   │           └── trace
│   │               └── bootstrap
│   │                   ├── AgentBootstrap.java
│   │                   └── AgentJar.java
```

## 各ソースの確認結果

### AgentBootstrap

- premainメソッドが実装されている。
- [datadog.trace.bootstrap.Agentのstartメソッド](002_agentBootstrap.md)を以下の引数で呼び出している。
  - startMethod.invoke(null, inst, agentJarURL, agentArgs);

- startMethod.invokeの第一引数に関する補足
  - インスタンスメソッドの場合は、対象のインスタンスを指定する。
  - staticメソッドの場合は、何を指定しても無視される（nullを指定することも可能）

詳細は`Method.invoke`のjavadocを参照

```java
  public static void agentmain(final String agentArgs, final Instrumentation inst) {
    if (alreadyInitialized() || lessThanJava8() || isJdkTool()) {
      return;
    }

    try {
      final URL agentJarURL = installAgentJar(inst);
      final Class<?> agentClass = Class.forName("datadog.trace.bootstrap.Agent", true, null);
      if (agentClass.getClassLoader() != null) {
        throw new IllegalStateException("DD Java Agent NOT added to bootstrap classpath.");
      }
      final Method startMethod =
          agentClass.getMethod("start", Instrumentation.class, URL.class, String.class);
      startMethod.invoke(null, inst, agentJarURL, agentArgs);
    } catch (final Throwable ex) {
      if (exceptionCauseChainContains(
          ex, "datadog.trace.util.throwable.FatalAgentMisconfigurationError")) {
        throw new Error(ex);
      }
      // Don't rethrow.  We don't have a log manager here, so just print.
      System.err.println("ERROR " + thisClass.getName());
      ex.printStackTrace();
    }
  }
```

### 参考 Method.invokeの[Javadoc](https://docs.oracle.com/javase/jp/8/docs/api/java/lang/reflect/Method.html#invoke-java.lang.Object-java.lang.Object...-)

```text
public Object invoke(Object obj,
                     Object... args)
              throws IllegalAccessException,
                     IllegalArgumentException,
                     InvocationTargetException
このMethodオブジェクトによって表される基本となるメソッドを、指定したオブジェクトに対して指定したパラメータで呼び出します。
個別のパラメータは、プリミティブ仮パラメータと一致させるために自動的にラップ解除され、
プリミティブおよび参照パラメータは両方とも必要に応じてメソッド呼出し変換の対象になります。

基本となるメソッドがstaticの場合、指定されたobj引数は無視されます。nullも指定できます。

基本となるメソッドによって要求される仮パラメータ数が0の場合、指定されたargs配列は長さ0またはnullになります。

基本となるメソッドがインスタンス・メソッドの場合、動的メソッド・ルックアップを使用して呼出しが行われ、ターゲット・オブジェクトの実行時の型に基づいてオーバーライドが実行されます(『Java言語仕様、第2版』のセクション15.12.4.4を参照)。

基本となるメソッドがstaticの場合、メソッドを宣言したクラスがまだ初期化されていないときは、このクラスが初期化されます。

パラメータ:
   obj - 基本となるメソッドの呼出し元のオブジェクト
   args - メソッド呼出しに使用される引数
戻り値:
   このオブジェクトが表すメソッドを、パラメータargsを使用してobjにディスパッチした結果

```

### AgentJar

mainメソッドが実装されている。


