# TracerInstaller

## 概要

## シーケンス図

```plantuml
box datadog.trace.bootstrap
    participant InstallDatadogTracerCallback
end box 

box datadog.trace.agent.tooling
    participant TracerInstaller
end box

box datadog.trace.api
    participant GlobalTracer
end box

box datadog.trace.core
    participant CoreTracer
    participant CoreTracerBuilder
end box 

box datadog.trace.bootstrap.instrumentation.api
    participant AgentTracer
end box


activate InstallDatadogTracerCallback
activate TracerInstaller

InstallDatadogTracerCallback -> TracerInstaller : installGlobalTracer
note right
    以下の引数を受け取る。
    SharedCommunicationObjects
    ProfilingContextIntegration
end note
activate TracerInstaller

TracerInstaller -> GlobalTracer : get() 
note right 
    CoreTracerのインスタンスではない場合（おそらく初回の場合）
end note

TracerInstaller -> CoreTracer : builder()
activate CoreTracer
note right
  builderを生成して返却
end note
CoreTracer -> CoreTracerBuilder : new
activate CoreTracerBuilder
CoreTracer --> TracerInstaller : CoreTracerBuilderのインスタンス
deactivate CoreTracer

TracerInstaller -> CoreTracerBuilder : sharedCommunicationObjects  // メンバー変数にセット
TracerInstaller -> CoreTracerBuilder : profilingContextIntegration //メンバー変数にセット
TracerInstaller -> CoreTracerBuilder : pollForTracerFlareRequests  //フラグをtrueにしている
TracerInstaller -> CoreTracerBuilder : pollForTracingConfiguration //フラグをtrueにしている
TracerInstaller -> CoreTracerBuilder : build
CoreTracerBuilder -> CoreTracer : new
activate CoreTracer
CoreTracerBuilder --> TracerInstaller : CoreTracerのインスタンス

TracerInstaller -> TracerInstaller : installGlobalTracer
note right
    生成したCoreTracerのインスタンスを引数に受け取る
end note
activate TracerInstaller
TracerInstaller -> GlobalTracer : registerIfAbsent(CoreTracerのインスタンス)
note right
    渡されたTracerをstaticメンバーに登録する
    補足
      GlobalTracerのメソッドはすべてstaticに定義されており、インスタンス化されず使われる。
      すでにCallbackが登録済みであれば、それらに対してinstalledを呼び出す。
end note

TracerInstaller -> AgentTracer : registerIfAbsent(CoreTracerのインスタンス)
note right
    渡されたTracerをstaticメンバ変数に登録する。
    補足
      AgentTracerのメソッドはすべてstaticに定義されており、インスタンス化されず使われる。
end note

```

## ソースコード

```java
package datadog.trace.agent.tooling;

import datadog.communication.ddagent.SharedCommunicationObjects;
import datadog.trace.api.Config;
import datadog.trace.api.GlobalTracer;
import datadog.trace.bootstrap.instrumentation.api.AgentTracer;
import datadog.trace.bootstrap.instrumentation.api.ProfilingContextIntegration;
import datadog.trace.core.CoreTracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TracerInstaller {
  private static final Logger log = LoggerFactory.getLogger(TracerInstaller.class);
  /** Register a global tracer if no global tracer is already registered. */
  public static synchronized void installGlobalTracer(
      SharedCommunicationObjects sharedCommunicationObjects,
      ProfilingContextIntegration profilingContextIntegration) {
    if (Config.get().isTraceEnabled() || Config.get().isCiVisibilityEnabled()) {
      if (!(GlobalTracer.get() instanceof CoreTracer)) {
        installGlobalTracer(
            CoreTracer.builder()
                .sharedCommunicationObjects(sharedCommunicationObjects)
                .profilingContextIntegration(profilingContextIntegration)
                .pollForTracerFlareRequests()
                .pollForTracingConfiguration()
                .build());
      } else {
        log.debug("GlobalTracer already registered.");
      }
    } else {
      log.debug("Tracing is disabled, not installing GlobalTracer.");
    }
  }

  public static void installGlobalTracer(final CoreTracer tracer) {
    try {
      GlobalTracer.registerIfAbsent(tracer);
      AgentTracer.registerIfAbsent(tracer);

      log.debug("Global tracer installed");
    } catch (final RuntimeException re) {
      log.warn("Failed to register tracer: {}", tracer, re);
    }
  }

  public static void forceInstallGlobalTracer(CoreTracer tracer) {
    try {
      log.warn("Overriding installed global tracer.  This is not intended for production use");

      GlobalTracer.forceRegister(tracer);
      AgentTracer.forceRegister(tracer);

      log.debug("Global tracer installed");
    } catch (final RuntimeException re) {
      log.warn("Failed to register tracer: {}", tracer, re);
    }
  }
}
```
