# DependencyService

## todo

全般調査

## 概要

## クラス図

```plantuml
class DependencyService {
    + void schedulePeriodicResolution()
    + void resolveOneDependency()
    + void installOn(Instrumentation instrumentation)
    + Collection<Dependency> drainDeterminedDependencies()
    + void addURL(URL url) 
    - URI convertToURI(URL location) 
    + void run()
    + void stop()
}
class DependencyResolverQueue
class Dependency
class AgentTaskScheduler as "AgentTaskScheduler.Scheduled<Runnable>"

DependencyService *-- DependencyResolverQueue
DependencyService *-- Dependency
DependencyService *-- AgentTaskScheduler

```

## シーケンス図

### DependencyService::コンストラクタ


```plantuml

box datadog.telemetry
    participant TelemetrySystem
end box

box datadog.telemetry.dependency
    participant DependencyService
end box

box datadog.trace.util
  participant AgentTaskScheduler
  participant AgentTaskSchedulerScheduled as "AgentTaskScheduler.Scheduled<Runnable>"
  participant Scheduled<<Scheduled<Runnable>>>
  participant ShutdownHook
end box

box java.lang
  participant Worker <<Thread>>
  participant Runtime
end box

activate TelemetrySystem
activate AgentTaskScheduler

TelemetrySystem -> DependencyService : new
activate DependencyService

DependencyService -> AgentTaskScheduler : scheduleAtFixedRate
activate AgentTaskScheduler

AgentTaskScheduler -> Scheduled : new
activate Scheduled
AgentTaskScheduler -> AgentTaskScheduler : scheduleTarget
activate AgentTaskScheduler

AgentTaskScheduler -> AgentTaskScheduler : prepareWorkQueue
activate AgentTaskScheduler
deactivate AgentTaskScheduler

AgentTaskScheduler -> Worker : new
activate Worker 

AgentTaskScheduler -> AgentTaskScheduler : newAgentThread
activate AgentTaskScheduler
deactivate AgentTaskScheduler

AgentTaskScheduler -> ShutdownHook : new
activate ShutdownHook

AgentTaskScheduler -> Runtime : getRuntime
AgentTaskScheduler -> Runtime : addShutdownHook(shutdownHook)

AgentTaskScheduler -> Worker : start
deactivate AgentTaskScheduler
AgentTaskScheduler --> DependencyService

DependencyService --> TelemetrySystem : コンストラクタ終了

TelemetrySystem -> DependencyService : installOn
activate DependencyService
note right
  LocationsCollectingTransformerが呼び出されるように、
  "Instrumentation"に登録する。
end note

```

### DependencyService::installOn

```plantuml

box datadog.telemetry
    participant TelemetrySystem
end box

box datadog.telemetry.dependency
    participant DependencyService
    participant LocationsCollectingTransformer
end box


box java.lang
  participant Instrumentation
end box

activate TelemetrySystem

TelemetrySystem -> DependencyService : new
activate DependencyService


TelemetrySystem -> DependencyService : installOn
activate DependencyService

DependencyService -> LocationsCollectingTransformer : new
activate LocationsCollectingTransformer

DependencyService -> Instrumentation : addTransformer(LocationsCollectingTransformer)
note right
  ClassLoadが発生するごとに
  LocationsCollectingTransformer::transformメソッドが呼ばれる
  ようになる。
end note


```

## ソース

```java
package datadog.telemetry.dependency;

import datadog.trace.util.AgentTaskScheduler;
import java.lang.instrument.Instrumentation;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Service that detects app dependencies from classloading by using a no-op class-file transformer
 */
public class DependencyService implements Runnable {

  private static final Logger log = LoggerFactory.getLogger(DependencyService.class);

  private final DependencyResolverQueue resolverQueue = new DependencyResolverQueue();

  private final BlockingQueue<Dependency> newDependencies = new LinkedBlockingQueue<>();

  private AgentTaskScheduler.Scheduled<Runnable> scheduledTask;

  public void schedulePeriodicResolution() {
    scheduledTask =
        AgentTaskScheduler.INSTANCE.scheduleAtFixedRate(
            AgentTaskScheduler.RunnableTask.INSTANCE, this, 0, 1000L, TimeUnit.MILLISECONDS);
  }

  public void resolveOneDependency() {
    List<Dependency> dependencies = resolverQueue.pollDependency();
    if (!dependencies.isEmpty()) {
      for (Dependency dependency : dependencies) {
        log.debug("Resolved dependency {}", dependency.name);
        newDependencies.add(dependency);
      }
    }
  }

  /**
   * Registers this service as a no-op class file transformer.
   *
   * @param instrumentation instrumentation instance to register on
   */
  public void installOn(Instrumentation instrumentation) {
    instrumentation.addTransformer(new LocationsCollectingTransformer(this));
  }

  public Collection<Dependency> drainDeterminedDependencies() {
    List<Dependency> list = new LinkedList<>();
    int drained = newDependencies.drainTo(list);
    if (drained > 0) {
      return list;
    }
    return Collections.emptyList();
  }

  public void addURL(URL url) {
    resolverQueue.queueURI(convertToURI(url));
  }

  private URI convertToURI(URL location) {
    URI uri = null;

    if (location.getProtocol().equals("vfs")) {
      // resolve jboss virtual file system
      try {
        uri = JbossVirtualFileHelper.getJbossVfsPath(location);
      } catch (RuntimeException rte) {
        log.debug("Error in call to getJbossVfsPath", rte);
        return null;
      }
    }

    if (uri == null) {
      try {
        uri = new URI(location.toString().replace(" ", "%20"));
      } catch (URISyntaxException e) {
        log.warn("Error converting URL to URI", e);
        // silently ignored
      }
    }

    return uri;
  }

  @Override
  public void run() {
    resolveOneDependency();
  }

  public void stop() {
    if (scheduledTask != null) {
      scheduledTask.cancel();
      scheduledTask = null;
    }
  }
}

```