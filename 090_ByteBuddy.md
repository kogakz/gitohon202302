


```java
import net.bytebuddy.agent.builder.AgentBuilder;
import net.bytebuddy.asm.Advice;
import net.bytebuddy.matcher.ElementMatchers;

public class MyAgent {
    public static AgentBuilder.Transformer forAdvice() {
        return new AgentBuilder.Transformer.ForAdvice()
                .include(MyAdvice.class.getClassLoader())
                .advice(ElementMatchers.named("someMethod"), MyAdvice.class.getName());
    }
}

public class SomeClass {
    public void someMethod() {
        // some code
    }
}

public class MyAdvice {
    @Advice.OnMethodEnter
    static void onEnter() {
        System.out.println("Entering method");
    }

    @Advice.OnMethodExit
    static void onExit() {
        System.out.println("Exiting method");
    }
}
}

```