# LocationsCollectingTransformer

## 概要

## クラス図

```plantuml
interface ClassFileTransformer

class LocationsCollectingTransformer implements ClassFileTransformer {
    + byte[] transform
}

class DependencyService
class SeenDomains as "DDCache<ProtectionDomain, Boolean>"

LocationsCollectingTransformer *-- DependencyService
LocationsCollectingTransformer *-- SeenDomains

```

## シーケンス

### コンストラクタ

```plantuml
box datadog.telemetry.dependency
    participant DependencyService
    participant LocationsCollectingTransformer <<ClassFileTransformer>>
end box

activate DependencyService
DependencyService -> LocationsCollectingTransformer : new
activate LocationsCollectingTransformer

note right
    DependencyServiceを保持する
end note
```

### transform

DependencyServiceにaddURLをするだけして、nullを返す。
このメソッド自体では変換したクラスのbyteコードは返さない。

```plantuml
box datadog.telemetry.dependency
    participant LocationsCollectingTransformer <<ClassFileTransformer>>
    participant DependencyService
end box

box datadog.trace.api.cache
  participant DDCache
end box

participant ProtectionDomain

activate DependencyService
activate LocationsCollectingTransformer

LocationsCollectingTransformer -> LocationsCollectingTransformer : addDependency
activate LocationsCollectingTransformer
note right
  ロードされたクラスが、仮にファイルサーバ上にあるjarファイルを読み込んでいた場合
  対象のクラスが含まれていたjarファイルのファイルパスを
  DependencyService::addURLに登録する
end note

LocationsCollectingTransformer -> ProtectionDomain : getCodeSource
ProtectionDomain --> LocationsCollectingTransformer : CodeSource

LocationsCollectingTransformer -> CodeSource : getLocation
CodeSource --> LocationsCollectingTransformer : URL

LocationsCollectingTransformer -> DependencyService : addURL

LocationsCollectingTransformer -> DDCache : computeIfAbsent


```

#### 補足(以下はcopilotに質問して)

[Javaセキュリティモデル]<https://docs.oracle.com/cd/E16340_01/core.1111/b56235/introjps.htm>

ProtectionDomain : セキュリティポリシーとパスをまとめたもの

- どこからロードしたコード（≒classと読み替えてもよさそう）か？ 
  - classが含まれていたjarファイルのファイルパス(例えば)
    - file:（ローカル・ファイル・システム上のファイル）
    - http://*.oracle.com（oracle.comのホスト上のファイル）
    - file:${j2ee.home}/lib/oc4j-internal.jar
- そのコード（≒class)で何を実行してもよいか。
  - アクセスできるファイルのパスとか
  - 読み込みはOK、書き込みはNGとか

上記のパス情報を持っているのがCoudeSourceクラスで、具体的なパスを返すのが`codeSource.getLocation()`。
[ProtectionDomain](https://docs.oracle.com/javase/8/docs/api/java/security/ProtectionDomain.html)
[CodeSource](https://docs.oracle.com/javase/8/docs/api/java/security/CodeSource.html)

## ソース

```java
package datadog.telemetry.dependency;

import datadog.trace.api.cache.DDCache;
import datadog.trace.api.cache.DDCaches;
import java.lang.instrument.ClassFileTransformer;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class LocationsCollectingTransformer implements ClassFileTransformer {
  private static final Logger log = LoggerFactory.getLogger(LocationsCollectingTransformer.class);

  private static final int MAX_CACHED_JARS = 1024;
  private final DependencyService dependencyService;
  private final DDCache<ProtectionDomain, Boolean> seenDomains =
      DDCaches.newFixedSizeWeakKeyCache(MAX_CACHED_JARS);

  public LocationsCollectingTransformer(DependencyService dependencyService) {
    this.dependencyService = dependencyService;
  }

  @Override
  public byte[] transform(
      ClassLoader loader,
      String className,
      Class<?> classBeingRedefined,
      ProtectionDomain protectionDomain,
      byte[] classfileBuffer) {
    if (protectionDomain != null) {
      seenDomains.computeIfAbsent(protectionDomain, this::addDependency);
    }
    // returning 'null' is the best way to indicate that no transformation has been done.
    return null;
  }

  private boolean addDependency(final ProtectionDomain domain) {
    final CodeSource codeSource = domain.getCodeSource();
    final URL location = codeSource != null ? codeSource.getLocation() : null;
    final ClassLoader classLoader = domain.getClassLoader();
    log.debug("New protection domain with location {} and class loader {}", location, classLoader);
    if (location != null) {
      dependencyService.addURL(location);
    }
    return true;
  }
}

```
