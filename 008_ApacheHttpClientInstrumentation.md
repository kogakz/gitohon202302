# DatadogAgentのinstrumentationの実装例

## ApacheHttpClientInstrumentation

### 概要

ApacheHttpClientInstrumentationは、Apache HttpClientのインスツルメンテーションを提供するクラスです。
このクラスは、Tracingインターフェースを実装し、CanShortcutTypeMatchingインターフェースを継承しています。
Apache HttpClientのexecuteメソッドをインストルメントし、スパンの作成と終了処理を行います。
また、異なるバージョンのexecuteメソッドに対応するため、複数のAdviceクラスを定義しています。

### ソースコード抜粋

```java
package datadog.trace.instrumentation.apachehttpclient;

import static datadog.trace.agent.tooling.bytebuddy.matcher.HierarchyMatchers.implementsInterface;
import static datadog.trace.agent.tooling.bytebuddy.matcher.NameMatchers.named;
import static net.bytebuddy.matcher.ElementMatchers.isMethod;
import static net.bytebuddy.matcher.ElementMatchers.takesArgument;
import static net.bytebuddy.matcher.ElementMatchers.takesArguments;

import com.google.auto.service.AutoService;
import datadog.trace.agent.tooling.Instrumenter;
import datadog.trace.bootstrap.instrumentation.api.AgentScope;
import net.bytebuddy.asm.Advice;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.implementation.bytecode.assign.Assigner;
import net.bytebuddy.matcher.ElementMatcher;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;

@AutoService(Instrumenter.class)
public class ApacheHttpClientInstrumentation extends Instrumenter.Tracing
    implements Instrumenter.CanShortcutTypeMatching {

  public ApacheHttpClientInstrumentation() {
    super("httpclient", "apache-httpclient", "apache-http-client");
  }

  @Override
  public boolean onlyMatchKnownTypes() {
    return isShortcutMatchingEnabled(false);
  }

  /**
   * ApacheHttpClientInstrumentationの既知の一致するタイプの配列を返します。
   * これらのタイプは、インストゥルメント化する必要のあるクラスを識別するために使用されます。
   *
   * @return 既知の一致するタイプの配列
   */
  @Override
  public String[] knownMatchingTypes() {
    return new String[] {
      "org.apache.http.impl.client.AbstractHttpClient",
      "software.amazon.awssdk.http.apache.internal.impl.ApacheSdkHttpClient",
      "org.apache.http.impl.client.AutoRetryHttpClient",
      "org.apache.http.impl.client.CloseableHttpClient",
      "org.apache.http.impl.client.ContentEncodingHttpClient",
      "org.apache.http.impl.client.DecompressingHttpClient",
      "org.apache.http.impl.client.DefaultHttpClient",
      "org.apache.http.impl.client.InternalHttpClient",
      "org.apache.http.impl.client.MinimalHttpClient",
      "org.apache.http.impl.client.SystemDefaultHttpClient",
      "com.netflix.http4.NFHttpClient",
      "com.amazonaws.http.apache.client.impl.SdkHttpClient"
    };
  }

  //継承元のインタフェース、クラス名？
  @Override
  public String hierarchyMarkerType() {
    return "org.apache.http.client.HttpClient";
  }

  //継承元クラスにマッチするかどうかをチェックするMathcherと思われる
  @Override
  public ElementMatcher<TypeDescription> hierarchyMatcher() {
    return implementsInterface(named(hierarchyMarkerType()));
  }

  // トレースを追加するためのヘルパークラスの名前
  @Override
  public String[] helperClassNames() {
    return new String[] {
      packageName + ".ApacheHttpClientDecorator",
      packageName + ".HttpHeadersInjectAdapter",
      packageName + ".HostAndRequestAsHttpUriRequest",
      packageName + ".HelperMethods",
      packageName + ".WrappingStatusSettingResponseHandler",
    };
  }

  @Override
  public void adviceTransformations(AdviceTransformation transformation) {
    // There are 8 execute(...) methods.  Depending on the version, they may or may not delegate to
    // eachother. Thus, all methods need to be instrumented.  Because of argument position and type,
    // some methods can share the same advice class.  The call depth tracking ensures only 1 span is
    // created

    //以下はByteBuddyのルールに関する説明
    // aapplyAdviceは該当するメソッドの前後に処理を加えるためのメソッド
    //   @Advice.OnMethodEnterを指定しているメソッドが、対象のメソッドを呼び出す前に実行される。
    //   @Advice.OnMethodExitを指定しているメソッドが、対象のメソッドを呼び出した後に実行される
    // named はメソッド名が一致するもの
    // takesArguments は引数の数が一致するもの
    // takesArgument  は具体的な引数の情報（名前など）が一致するもの
    // 後にあるほうが優先されるため、後にあるメソッドのほうがより具体的な条件になるように書く必要がある。

    // transformation.applyAdive(ElementMatcher, 名前 )
    transformation.applyAdvice(
        isMethod()
            .and(named("execute"))
            .and(takesArguments(1)) //引数の数をチェック
            .and(takesArgument(0, named("org.apache.http.client.methods.HttpUriRequest"))), //第１引数の名前が該当すること
        ApacheHttpClientInstrumentation.class.getName() + "$UriRequestAdvice");

    transformation.applyAdvice(
        isMethod()
            .and(named("execute"))
            .and(takesArguments(2))
            .and(takesArgument(0, named("org.apache.http.client.methods.HttpUriRequest")))
            .and(takesArgument(1, named("org.apache.http.protocol.HttpContext"))),
        ApacheHttpClientInstrumentation.class.getName() + "$UriRequestAdvice");

    transformation.applyAdvice(
        isMethod()
            .and(named("execute"))
            .and(takesArguments(2))
            .and(takesArgument(0, named("org.apache.http.client.methods.HttpUriRequest")))
            .and(takesArgument(1, named("org.apache.http.client.ResponseHandler"))),
        ApacheHttpClientInstrumentation.class.getName() + "$UriRequestWithHandlerAdvice");

    transformation.applyAdvice(
        isMethod()
            .and(named("execute"))
            .and(takesArguments(3))
            .and(takesArgument(0, named("org.apache.http.client.methods.HttpUriRequest")))
            .and(takesArgument(1, named("org.apache.http.client.ResponseHandler")))
            .and(takesArgument(2, named("org.apache.http.protocol.HttpContext"))),
        ApacheHttpClientInstrumentation.class.getName() + "$UriRequestWithHandlerAdvice");

    transformation.applyAdvice(
        isMethod()
            .and(named("execute"))
            .and(takesArguments(2))
            .and(takesArgument(0, named("org.apache.http.HttpHost")))
            .and(takesArgument(1, named("org.apache.http.HttpRequest"))),
        ApacheHttpClientInstrumentation.class.getName() + "$RequestAdvice");

    transformation.applyAdvice(
        isMethod()
            .and(named("execute"))
            .and(takesArguments(3))
            .and(takesArgument(0, named("org.apache.http.HttpHost")))
            .and(takesArgument(1, named("org.apache.http.HttpRequest")))
            .and(takesArgument(2, named("org.apache.http.protocol.HttpContext"))),
        ApacheHttpClientInstrumentation.class.getName() + "$RequestAdvice");

    transformation.applyAdvice(
        isMethod()
            .and(named("execute"))
            .and(takesArguments(3))
            .and(takesArgument(0, named("org.apache.http.HttpHost")))
            .and(takesArgument(1, named("org.apache.http.HttpRequest")))
            .and(takesArgument(2, named("org.apache.http.client.ResponseHandler"))),
        ApacheHttpClientInstrumentation.class.getName() + "$RequestWithHandlerAdvice");

    transformation.applyAdvice(
        isMethod()
            .and(named("execute"))
            .and(takesArguments(4))
            .and(takesArgument(0, named("org.apache.http.HttpHost")))
            .and(takesArgument(1, named("org.apache.http.HttpRequest")))
            .and(takesArgument(2, named("org.apache.http.client.ResponseHandler")))
            .and(takesArgument(3, named("org.apache.http.protocol.HttpContext"))),
        ApacheHttpClientInstrumentation.class.getName() + "$RequestWithHandlerAdvice");
  }

  public static class UriRequestAdvice {
    @Advice.OnMethodEnter(suppress = Throwable.class)
    public static AgentScope methodEnter(@Advice.Argument(0) final HttpUriRequest request) {
      return HelperMethods.doMethodEnter(request);
    }

    @Advice.OnMethodExit(onThrowable = Throwable.class, suppress = Throwable.class)
    public static void methodExit(
        @Advice.Enter final AgentScope scope,
        @Advice.Return final Object result,
        @Advice.Thrown final Throwable throwable) {
      HelperMethods.doMethodExit(scope, result, throwable);
    }
  }

  public static class UriRequestWithHandlerAdvice {

    @Advice.OnMethodEnter(suppress = Throwable.class)
    public static AgentScope methodEnter(
        @Advice.Argument(0) final HttpUriRequest request,
        @Advice.Argument(
                value = 1,
                optional = true,
                typing = Assigner.Typing.DYNAMIC,
                readOnly = false)
            Object handler) {
      final AgentScope scope = HelperMethods.doMethodEnter(request);
      // Wrap the handler so we capture the status code
      if (null != scope && handler instanceof ResponseHandler) {
        handler = new WrappingStatusSettingResponseHandler(scope.span(), (ResponseHandler) handler);
      }
      return scope;
    }

    @Advice.OnMethodExit(onThrowable = Throwable.class, suppress = Throwable.class)
    public static void methodExit(
        @Advice.Enter final AgentScope scope,
        @Advice.Return final Object result,
        @Advice.Thrown final Throwable throwable) {
      HelperMethods.doMethodExit(scope, result, throwable);
    }
  }

  public static class RequestAdvice {
    @Advice.OnMethodEnter(suppress = Throwable.class)
    public static AgentScope methodEnter(
        @Advice.Argument(0) final HttpHost host, @Advice.Argument(1) final HttpRequest request) {
      if (request instanceof HttpUriRequest) {
        return HelperMethods.doMethodEnter((HttpUriRequest) request);
      } else {
        return HelperMethods.doMethodEnter(host, request);
      }
    }

    @Advice.OnMethodExit(onThrowable = Throwable.class, suppress = Throwable.class)
    public static void methodExit(
        @Advice.Enter final AgentScope scope,
        @Advice.Return final Object result,
        @Advice.Thrown final Throwable throwable) {
      HelperMethods.doMethodExit(scope, result, throwable);
    }
  }

  public static class RequestWithHandlerAdvice {

    @Advice.OnMethodEnter(suppress = Throwable.class)
    public static AgentScope methodEnter(
        @Advice.Argument(0) final HttpHost host,
        @Advice.Argument(1) final HttpRequest request,
        @Advice.Argument(
                value = 2,
                optional = true,
                typing = Assigner.Typing.DYNAMIC,
                readOnly = false)
            Object handler) {
      final AgentScope scope;
      if (request instanceof HttpUriRequest) {
        scope = HelperMethods.doMethodEnter((HttpUriRequest) request);
      } else {
        scope = HelperMethods.doMethodEnter(host, request);
      }
      // Wrap the handler so we capture the status code
      if (null != scope && handler instanceof ResponseHandler) {
        handler = new WrappingStatusSettingResponseHandler(scope.span(), (ResponseHandler) handler);
      }
      return scope;
    }

    @Advice.OnMethodExit(onThrowable = Throwable.class, suppress = Throwable.class)
    public static void methodExit(
        @Advice.Enter final AgentScope scope,
        @Advice.Return final Object result,
        @Advice.Thrown final Throwable throwable) {
      HelperMethods.doMethodExit(scope, result, throwable);
    }
  }
}
```

## HelperMethods

### HelperMethodsの概要

ApacheHttpClientInstrumentationのヘルパー

### HelperMethodsのソースコード

```java
package datadog.trace.instrumentation.apachehttpclient;

import static datadog.trace.bootstrap.instrumentation.api.AgentTracer.activateSpan;
import static datadog.trace.bootstrap.instrumentation.api.AgentTracer.propagate;
import static datadog.trace.bootstrap.instrumentation.api.AgentTracer.startSpan;
import static datadog.trace.instrumentation.apachehttpclient.ApacheHttpClientDecorator.DECORATE;
import static datadog.trace.instrumentation.apachehttpclient.ApacheHttpClientDecorator.HTTP_REQUEST;
import static datadog.trace.instrumentation.apachehttpclient.HttpHeadersInjectAdapter.SETTER;

import datadog.trace.api.Config;
import datadog.trace.bootstrap.CallDepthThreadLocalMap;
import datadog.trace.bootstrap.instrumentation.api.AgentScope;
import datadog.trace.bootstrap.instrumentation.api.AgentSpan;
import datadog.trace.bootstrap.instrumentation.decorator.HttpClientDecorator;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;

public class HelperMethods {
  private static final boolean AWS_LEGACY_TRACING =
      Config.get().isLegacyTracingEnabled(false, "aws-sdk");

  // 
  public static AgentScope doMethodEnter(final HttpUriRequest request) {
    boolean awsClientCall = request.containsHeader("amz-sdk-invocation-id");
    if (!AWS_LEGACY_TRACING && awsClientCall) {
      // avoid creating an extra HTTP client span beneath the AWS client call
      return null;
    }

    final int callDepth = CallDepthThreadLocalMap.incrementCallDepth(HttpClient.class);
    if (callDepth > 0) {
      return null;
    }

    return activateHttpSpan(request, awsClientCall);
  }

  public static AgentScope doMethodEnter(HttpHost host, HttpRequest request) {
    boolean awsClientCall = request.containsHeader("amz-sdk-invocation-id");
    if (!AWS_LEGACY_TRACING && awsClientCall) {
      // avoid creating an extra HTTP client span beneath the AWS client call
      return null;
    }

    final int callDepth = CallDepthThreadLocalMap.incrementCallDepth(HttpClient.class);
    if (callDepth > 0) {
      return null;
    }

    return activateHttpSpan(new HostAndRequestAsHttpUriRequest(host, request), awsClientCall);
  }

  private static AgentScope activateHttpSpan(
      final HttpUriRequest request, final boolean awsClientCall) {
    final AgentSpan span = startSpan(HTTP_REQUEST);
    final AgentScope scope = activateSpan(span);

    DECORATE.afterStart(span);
    DECORATE.onRequest(span, request);

    // AWS calls are often signed, so we can't add headers without breaking the signature.
    if (!awsClientCall) {
      propagate().inject(span, request, SETTER);
      propagate()
          .injectPathwayContext(
              span, request, SETTER, HttpClientDecorator.CLIENT_PATHWAY_EDGE_TAGS);
    }

    return scope;
  }

  public static void doMethodExit(
      final AgentScope scope, final Object result, final Throwable throwable) {
    if (scope == null) {
      return;
    }
    final AgentSpan span = scope.span();
    try {
      if (result instanceof HttpResponse) {
        DECORATE.onResponse(span, (HttpResponse) result);
      } // else they probably provided a ResponseHandler.

      DECORATE.onError(span, throwable);
      DECORATE.beforeFinish(span);
    } finally {
      scope.close();
      span.finish();
      CallDepthThreadLocalMap.reset(HttpClient.class);
    }
  }
}
```
