# AgentBuilder

[AgentBulderのJavadoc のリンク](https://javadoc.io/doc/net.bytebuddy/byte-buddy/latest/net/bytebuddy/agent/builder/AgentBuilder.html)

実際に利用している設定に関するドキュメントを抜粋

```java
    AgentBuilder agentBuilder =
        //Creates a new agent builder with default settings.
        new AgentBuilder.Default(byteBuddy)

        //Disables all implicit changes on a class file that Byte Buddy would apply for certain instrumentations.
        //Byte Buddy が特定のインスツルメンテーションに適用するクラスファイルの暗黙的な変更をすべて無効にします。
            .disableClassFormatChanges()

        //Assures that all modules of the supplied types are read by the module of any instrumented type.
        //  提供されたタイプのすべてのモジュールが、計測されたタイプのモジュールによって確実に読み取られるようにします
            .assureReadEdgeTo(inst, FieldBackedContextAccessor.class)

        // Adds a decorator for the created class file transformer.
        // 作成されたクラス ファイル トランスフォーマーのデコレータを追加します。
            .with(AgentStrategies.transformerDecorator())

        // すでにロードされているすべてのクラス、
        // およびビルドされたエージェントがロードされる前に登録されていた場合に
        // 変換されたであろうすべてのクラスに再変換を適用します
            .with(AgentBuilder.RedefinitionStrategy.RETRANSFORMATION)

        // パラメータに渡されたクラスをつかって、再検出する。
        // DDRediscoveryStrategy
            .with(AgentStrategies.rediscoveryStrategy())

        //  今後調査
            .with(redefinitionStrategyListener(enabledSystems))
            .with(AgentStrategies.locationStrategy())
            .with(AgentStrategies.poolStrategy())
            .with(AgentBuilder.DescriptionStrategy.Default.POOL_ONLY)
            .with(AgentStrategies.bufferStrategy())
            .with(AgentStrategies.typeStrategy())
            .with(new ClassLoadListener())
            // FIXME: we cannot enable it yet due to BB/JVM bug, see
            // https://github.com/raphw/byte-buddy/issues/558
            // .with(AgentBuilder.LambdaInstrumentationStrategy.ENABLED)
            .ignore(globalIgnoresMatcher(skipAdditionalLibraryMatcher));

```

