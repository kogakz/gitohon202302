

## 参考サイト
Datadog APM のリポジトリ : https://github.com/DataDog/dd-trace-java

## ビルド手順
以下を参考
https://github.com/DataDog/dd-trace-java/blob/master/CONTRIBUTING.md

以下を環境変数にセットする。
 - JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
 - JAVA_17_HOME=/usr/lib/jvm/java-17-openjdk-amd64
 - JAVA_11_HOME=/usr/lib/jvm/java-11-openjdk-amd64
 - JAVA_8_HOME=/usr/lib/jvm/java-8-openjdk-amd64

以下のコマンドを実行する。

```shell
./gradlew clean assemble
```

## フォルダ構成

```text
├── benchmark
├── buildSrc
├── communication
├── dd-java-agent メインソースが掛かれているっぽい
├── dd-smoke-tests
├── dd-trace-api
├── dd-trace-core
├── dd-trace-ot
├── gradle
├── internal-api
├── lib-injection
├── remote-config
├── telemetry
├── test-published-dependencies
├── tooling
└── utils
```

## dd-java-agent配下のソース

~~~text
dd-java-agent
├── README.md
├── agent-bootstrap
├── agent-builder
├── agent-ci-visibility
├── agent-crashtracking
├── agent-debugger
├── agent-iast
├── agent-jmxfetch
├── agent-logging
├── agent-profiling
├── agent-tooling
├── appsec
├── benchmark
├── benchmark-integration
├── build
├── build.gradle
├── cws-tls
├── instrumentation
├── load-generator
├── src
│   ├── main
│   │   └── java
│   │       └── datadog
│   │           └── trace
│   │               └── bootstrap
│   │                   ├── AgentBootstrap.java
│   │                   └── AgentJar.java
│   省略
└── testing
```

## 確認対象、状況

[bootstrap](001_bootstrap.md)

進捗（0/2) 

[]


