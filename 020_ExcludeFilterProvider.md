# ExcludeFilterProvider

除外するクラスを管理する

```java
public interface ExcludeFilterProvider {

  /**
   * @return A mapping from {@link ExcludeType} -> {@link Set<String>} for the class names that
   *     should be excluded from broad instrumentations like {@link Runnable}
   */
  Map<ExcludeType, ? extends Collection<String>> excludedClasses();
}
```

## リスト

```shell-session{.line-numbers}
grep -rl --include='*.java' 'ExcludeFilterProvider' .
./dd-java-agent/agent-builder/src/main/java/datadog/trace/agent/tooling/AgentInstaller.java
./dd-java-agent/testing/src/test/java/excludefilter/ExcludeFilterTestInstrumentation.java
./dd-java-agent/agent-tooling/src/main/java/datadog/trace/agent/tooling/ExcludeFilterProvider.java
./dd-java-agent/instrumentation/graal/native-image/src/main/java/datadog/trace/instrumentation/graal/nativeimage/VMRuntimeInstrumentation.java
./dd-java-agent/instrumentation/jetty-11/src/main/java/datadog/trace/instrumentation/jetty11/JettyServerInstrumentation.java
./dd-java-agent/instrumentation/tomcat-5.5/src/main/java/datadog/trace/instrumentation/tomcat/TomcatServerInstrumentation.java
./dd-java-agent/instrumentation/akka-concurrent/src/main/java/datadog/trace/instrumentation/akka/concurrent/AkkaMailboxInstrumentation.java
./dd-java-agent/instrumentation/akka-concurrent/src/main/java/datadog/trace/instrumentation/akka/concurrent/AkkaForkJoinTaskInstrumentation.java
./dd-java-agent/instrumentation/jetty-12/src/main/java/datadog/trace/instrumentation/jetty12/JettyServerInstrumentation.java
./dd-java-agent/instrumentation/jetty-client-9.1/src/main/java/datadog/trace/instrumentation/jetty_client/JettyClientInstrumentation.java
./dd-java-agent/instrumentation/scala-concurrent/src/main/java/datadog/trace/instrumentation/scala/concurrent/ScalaForkJoinTaskInstrumentation.java
./dd-java-agent/instrumentation/google-pubsub/src/main/java/datadog/trace/instrumentation/googlepubsub/PublisherInstrumentation.java
./dd-java-agent/instrumentation/scala-promise/scala-promise-2.10/src/main/java/datadog/trace/instrumentation/scala210/concurrent/CallbackRunnableInstrumentation.java
./dd-java-agent/instrumentation/scala-promise/scala-promise-2.13/src/main/java/datadog/trace/instrumentation/scala213/concurrent/PromiseTransformationInstrumentation.java
./dd-java-agent/instrumentation/pekko-concurrent/src/main/java/datadog/trace/instrumentation/pekko/concurrent/PekkoMailboxInstrumentation.java
./dd-java-agent/instrumentation/spring-rabbit/src/main/java/datadog/trace/instrumentation/springamqp/AbstractMessageListenerContainerInstrumentation.java
./dd-java-agent/instrumentation/jetty-9/src/main/java/datadog/trace/instrumentation/jetty9/JettyServerInstrumentation.java
./dd-java-agent/instrumentation/jetty-9/src/main/java/datadog/trace/instrumentation/jetty10/JettyServerInstrumentation.java
./dd-java-agent/instrumentation/java-concurrent/src/main/java/datadog/trace/instrumentation/java/concurrent/RunnableFutureInstrumentation.java
./dd-java-agent/instrumentation/java-concurrent/src/main/java/datadog/trace/instrumentation/java/concurrent/ThreadPoolExecutorInstrumentation.java
./dd-java-agent/instrumentation/java-concurrent/src/main/java/datadog/trace/instrumentation/java/concurrent/JavaForkJoinTaskInstrumentation.java
./dd-java-agent/instrumentation/java-concurrent/java-completablefuture/src/main/java/datadog/trace/instrumentation/java/completablefuture/CompletableFutureUniCompletionInstrumentation.java
./dd-java-agent/instrumentation/java-concurrent/java-completablefuture/src/main/java/datadog/trace/instrumentation/java/completablefuture/AsyncTaskInstrumentation.java
```

## サンプル

### TomcatServerInstrumentation

```java{.line-numbers}
@Override
public Map<ExcludeFilter.ExcludeType, ? extends Collection<String>> excludedClasses() {
return singletonMap(
    RUNNABLE,
    Arrays.asList(
        "org.apache.tomcat.util.threads.TaskThread$WrappingRunnable",
        "org.apache.tomcat.util.net.SocketProcessorBase",
        "org.apache.tomcat.util.net.AprEndpoint$Poller",
        "org.apache.tomcat.util.net.NioEndpoint$Poller",
        "org.apache.tomcat.util.net.NioEndpoint$PollerEvent",
        "org.apache.tomcat.util.net.AprEndpoint$SocketProcessor",
        "org.apache.tomcat.util.net.JIoEndpoint$SocketProcessor",
        "org.apache.tomcat.util.net.NioEndpoint$SocketProcessor",
        "org.apache.tomcat.util.net.Nio2Endpoint$SocketProcessor",
        "org.apache.tomcat.util.net.NioBlockingSelector$BlockPoller"));
}
```

#### ThreadPoolExecutorInstrumentation

```java{.line-numbers}
@Override
public Map<ExcludeFilter.ExcludeType, ? extends Collection<String>> excludedClasses() {
return Collections.singletonMap(
    RUNNABLE,
    Arrays.asList(
        "datadog.trace.bootstrap.instrumentation.java.concurrent.Wrapper",
        "datadog.trace.bootstrap.instrumentation.java.concurrent.ComparableRunnable"));
}
```
