# TransfomerBuilder

AgentInstallerがInstrumenterのinstrumentを呼び出すとTransfomerBuilderのapplyInstrumentationが呼び出される。
内部的には、継承先クラスのbuildInstrumentationが呼ばれる流れになっている。

継承先クラス

- LegacyTransformerBuilder
- CombiningTransformerBuilder

## AbstractTransformerBuilder

Instrumenter内で定義されているインタフェース`TransformerBuilder`,`AdviceTransformation`をimplementsしています。

また、AbstractTransformerBuilderを継承している実態クラスは、次の2クラスでした。

- LegacyTransformerBuilder
- CombiningTransformerBuilder

```plantuml

set namespaceSeparator none

package net.bytebuddy.agent.builder.AgentBuilder {
    interface Transformer
}

package Datadog{
    abstract class AbstractTransformerBuilder {
        # {abstract} void buildInstrumentation(Instrumenter.Default)
        # {abstract} void buildSingleAdvice(Instrumenter.ForSingleType)
        # {abstract} void applyContextStoreInjection(contextStore, ElementMatcher<ClassLoader>)
        # void applyContextStoreInjection()
        # void registerContextStoreInjection(Instrumenter.Default, contextStore)
        # int contextStoreCount()
        # ElementMatcher<ClassLoader> requireBoth(ElementMatcher<ClassLoader>, ElementMatcher<ClassLoader>)
        // registerContextStoreInjectionの中で使われている。

        + void applyInstrumentation(Instrumenter.HasAdvice) 
    }

    note top of AbstractTransformerBuilder
    - "javax.decorator.Decorator", "jakarta.decorator.Decorator"アノテーションがついているものは対象外
    end note

    note left of AbstractTransformerBuilder::applyInstrumentation
        TransfomerBuilder インタフェースのメソッド
    end note

    class VisitingTransformer{
        # transform() //引数は省略
    }
    
    class HelperTransformer implements Transformer 
    class VisitingTransformer implements Transformer 
     class HelperTransformer extends HelperInjector
}
AbstractTransformerBuilder +--- VisitingTransformer
AbstractTransformerBuilder +--- HelperTransformer




```

class HelperInjector implements AdviceTransformer

### AbstractTransformerBuilder::applyInstrumentationの処理内容

Instrumenterの型で、次のメソッドを呼び出す

- `Instrumenter.Default` : `buildInstrumentation(Instrumenter)`
- `Instrumenter.ForSingleType`：`buildSingleAdvice(Instrumenter)`

なお、両方とも抽象メソッドのため実際のメソッド定義は、その継承先を見る必要がある。

- `CombiningTransformerBuilder`
- `LegacyTransformerBuilder`

#### LegacyTransformerBuilder.buildInstrumentation

```plantuml
box datadog.trace.agent.tooling
    participant LegacyTransformerBuilder
    participant InstrumenterState
end box

box  net.bytebuddy.agent.builder
    participant AgentBuilder
end box

LegacyTransformerBuilder -> InstrumenterState : registerInstrumentation instrumenter
LegacyTransformerBuilder -> LegacyTransformerBuilder : typeMather
```

```java
public final class LegacyTransformerBuilder extends AbstractTransformerBuilder {

    private AgentBuilder agentBuilder;
    private ElementMatcher<? super MethodDescription> ignoreMatcher;

    // AgentBuilder.Identified.Extendable のjavadoc抜粋
    //   This interface is used to allow for optionally providing several AgentBuilder.Transformer 
    //   when a matcher identifies a type to be instrumented
    // instrumentする型をmatcherで識別するのに使うっぽい。
    private AgentBuilder.Identified.Extendable adviceBuilder;

    LegacyTransformerBuilder(AgentBuilder agentBuilder) {
    this.agentBuilder = agentBuilder;
    }
  
  //省略

    protected void buildInstrumentation(Instrumenter.Default instrumenter) { 
        InstrumenterState.registerInstrumentation(instrumenter);

        ignoreMatcher = instrumenter.methodIgnoreMatcher();
        // bytebuddyにjavadocを読んだが、理解できなかった。
        adviceBuilder =
            agentBuilder
                .type(typeMatcher(instrumenter)) //Instrumenterが対応する型のAgentBuilderを返す（多分）
                .and(NOT_DECORATOR_MATCHER)      //
                .and(new MuzzleMatcher(instrumenter))
                .transform(defaultTransformers()); // defaultTransformers()を適用した結果のAgentBuilderを返す。

        //package datadog.trace.instrumentation.apachehttpclient.ApacheHttpClientInstrumentation
        // 実装を見ると以下の定義がされていて、同じパッケージに対応するクスラスがあった。
        // 
        //　packageName + ".ApacheHttpClientDecorator",
        //  packageName + ".HttpHeadersInjectAdapter",
        //  packageName + ".HostAndRequestAsHttpUriRequest",
        //  packageName + ".HelperMethods",
        //  packageName + ".WrappingStatusSettingResponseHandler",
        String[] helperClassNames = instrumenter.helperClassNames();
        if (instrumenter.injectHelperDependencies()) {
            helperClassNames = HelperScanner.withClassDependencies(helperClassNames);
        }
        //ヘルパークラスのリストに対象クラスを持っている場合は、AgentBuilderに対象のヘルパーを適用していく
        if (helperClassNames.length > 0) {
            adviceBuilder = adviceBuilder.transform(
                new HelperTransformer(instrumenter.getClass().getSimpleName(), helperClassNames));
        }

        // このインスツルメンテーションに対して定義するコンテキスト ストア。一致するクラスローダーに追加されます。
        // A map of {class-name -> context-class-name}. 
        
        // 実際の変換は、FieldBackedContextRequestRewriter.wrapメソッドで行われる。
        // FieldBackedContextRequestRewriterのクラス説明には、変換中に ContextStore ID を割り当て
        // それを使用して実行時に ContextStore インスタンスを取得することによって、InstrumentationContext 呼び出しを書き換えると書いてあった。
        // wrapメソッドは、context-class-nameの対象クラスのメソッドを呼び出すようにASMを使ってメソッドを組み立てている。っぽい。
        Map<String, String> contextStore = instrumenter.contextStore();
        if (!contextStore.isEmpty()) {
            // rewrite context store access to call FieldBackedContextStores with assigned store-id
            adviceBuilder = adviceBuilder.transform(
                                new VisitingTransformer(  // Instrumenter.java内に定義されているAdviceTransformer インタフェースの拡張クラス
                                    //引数でもらうAsmVisitorWrapperを受けて、それに基づいてクラス書き換えを行う。
                                    new FieldBackedContextRequestRewriter(contextStore, instrumenter.name())
                                )
                            );


            // どのクラスローダーマッチャーが各ストアリクエストに関連付けられているかを追跡します
            registerContextStoreInjection(instrumenter, contextStore);

        }

        // instrumenter.transformerを呼び出すとAdviceTransformerが返ってくる。
        // ※AdviceTransformerはInstrumenterに定義されているインタフェースで、
        //   AdviceTransformer.transformを呼び出すと、DynamicType.Builderを返却する
        // AdviceTransformer
        // implements Instrumenter.AdviceTransformerで検索すると
        // - datadog.trace.agent.tooling.HelperInjector
        // - datadog.trace.agent.tooling.bytebuddy.csi.CallSiteTransformer
        // の2種類が実装されている
        final Instrumenter.AdviceTransformer customTransformer = instrumenter.transformer();
        if (customTransformer != null) {
            adviceBuilder = adviceBuilder.transform(customTransformer::transform);
        }

        // todo このメソッド周りをシーケンスにする
        agentBuilder = registerAdvice(instrumenter);
    }

    private AgentBuilder registerAdvice(Instrumenter.HasAdvice instrumenter) {

        // AdviceTransformation#applyAdvice(ElementMatcher, String) を呼び出す。
        // /dd-trace-java/dd-java-agent/instrumentationのinstrumenter実装クラスで
        // adviceTransformationsはOverrideされていた。
        instrumenter.adviceTransformations(this);
        return adviceBuilder;
    }

    @Override
    public void applyAdvice(ElementMatcher<? super MethodDescription> matcher, String name) {
        adviceBuilder = adviceBuilder.transform(
            new AgentBuilder.Transformer.ForAdvice()
                .include(Utils.getBootstrapProxy(), Utils.getAgentClassLoader())
                .withExceptionHandler(ExceptionHandlers.defaultExceptionHandler())
                .advice(not(ignoreMatcher).and(matcher), name));
    }

    @Override
    protected void buildSingleAdvice(Instrumenter.ForSingleType instrumenter) {
        AgentBuilder.RawMatcher matcher = new SingleTypeMatcher(instrumenter.instrumentedType());

        ignoreMatcher = isSynthetic();
        adviceBuilder = agentBuilder.type(matcher).and(NOT_DECORATOR_MATCHER).transform(defaultTransformers());

        agentBuilder = registerAdvice((Instrumenter.HasAdvice) instrumenter);
    }
```

## CombiningTransformerBuilder

### 概要

### クラス図

```plantuml
class CombiningTransformerBuilder{

    knownTBitSet knownTypesMask  // InstrumenterがForKnownTypesの場合IDを登録
    List<MatchRecorder> matchers //  InstrumenterがForTypeHierarchy,ForCallSite,ForConfiguredTypesに一致する場合 対応するMatchRecorderをIDとともに登録
    AdviceStack[] transformers   // 

    # void : buildInstrumentation(Instrumenter.Default instrumenter) // insturmenterから呼ばれる入口
    + CombiningTransformerBuilder(AgentBuilder agentBuilder, int maxInstrumentationId)  // コンストラクタ
    + void : applyAdvice(ElementMatcher<? super MethodDescription> matcher, String name)
    + ClassFileTransformer : installOn(Instrumentation instrumentation) //TransfromerをJVMに登録する
}
```

### シーケンス図

#### CombiningTransformerBuilder::applyInstrumentation(Instrumenter.HasAdvice instrumenter)のシーケンス

```plantuml
activate Instrumenter
Instrumenter -> CombiningTransformerBuilder : applyInstrumentation
activate CombiningTransformerBuilder

alt instrumenter instanceof Instrumenter.Default
    ref over CombiningTransformerBuilder
        CombiningTransformerBuilder::buildInstrumentationのシーケンス参照
    end ref

else instrumenter instanceof Instrumenter.ForSingleType
    ref over CombiningTransformerBuilder
        CombiningTransformerBuilder::buildInstrumentationのシーケンス参照
    end ref
end 

```

#### CombiningTransformerBuilder::buildInstrumentationのシーケンス

```plantuml
activate Insrumenter
activate CombiningTransformerBuilder
activate InstrumenterState

Insrumenter -> CombiningTransformerBuilder : buildInstrumentation

CombiningTransformerBuilder -> InstrumenterState : registerInstrumentation

note right CombiningTransformerBuilder
    **buildInstrumentationMatcher**は
    Instrumnenterのインタフェース実装などに応じたmatcherの登録を行う。
    様々なMatcherを定義していたが煩雑なため割愛。
end note
CombiningTransformerBuilder -> CombiningTransformerBuilder : buildInstrumentationMatcher
activate CombiningTransformerBuilder
deactivate CombiningTransformerBuilder

CombiningTransformerBuilder -> CombiningTransformerBuilder : buildInstrumentationAdvice
activate CombiningTransformerBuilder
deactivate CombiningTransformerBuilder

CombiningTransformerBuilder --> Insrumenter


```

##### buildInstrumentationMatcherでやっていたことの簡易メモ

- ForSingleType:実装している場合
- ForKnownTypesを実装している場合
  - knownTypesMaskにidをセットする

以下は、matchersに登録するMatchRecorderの説明のみ行う。
- ForTypeHierarchyをサポートする場合
  - MatchRecorder.ForHierarchy(id, instrumenter)
- ForCallSiteをサポートする場合
  - MatchRecorder.ForType(id, instrumenter.callerType())
- ForConfiguredTypesを実装している場合
  - MatchRecorder.ForType(id, namedOneOf(names))を登録
- CanShortcutTypeMatchingを実装していて、instrumenter.onlyMatchKnownTypes()ではない場合
  - MatchRecorder.ForHierarchy(id, (Instrumenter.ForTypeHierarchy) instrumenter)
- InsturmenterのclassLoaderMatcherが、デフォルト設定ではない場合
  - MatchRecorder.NarrowLocation(id, classLoaderMatcher)
- InstrumenterのインタフェースがInstrumenter.WithTypeStructure
  - MatchRecorder.NarrowType(id, ((Instrumenter.WithTypeStructure) instrumenter).structureMatcher())

- 最後に必ず以下を入れる
  - matchers.add(new MatchRecorder.NarrowLocation(id, new MuzzleCheck(instrumenter)));

#### CombiningTransformerBuilder::buildInstrumentationAdviceの概要とシーケンス

- buildInstrumentationから呼ばれる
- InsturmenterがAdviceを使ったTransformerを生成する
- 生成されたtransformerは、TransformerBuilderのtransformersに登録される。
  - 一つのInsturmenterが複数のTransformerを生成することがあるため、transfomersにはstackとして保持されている
- CombiningTransformerBuilder::installOnメソッドを呼び出すとtransformersに登録されたActiveStackのtransformが呼び出される。

```plantuml

component Instrumenter [
    Instumenter
    Transfomerを生成する
]
node CombiningTransformerBuilder  {

    node transfomers <<ActiveStack[]>> {    
        node ActiveStack <<AgentBuilder.Transformer>> {
            node AgentBuilder.Transformerの配列{
                component Transfomer
            }
        }
    }
}

Instrumenter -> Transfomer : 生成&登録
note right
    - transfomersはInstrumenterの数だけActiveStackを保持する

    - Insturmenter:ActiveStack:Transfomer = 1:1:N の関係
      - Instrumenterは複数のTransformerを生成ケースがあるため
        複数のTransfomerを管理するためにActiveStackで管理

    ActiveStack::transformメソッドを呼び出すと保持するTransfomerの数だけ
    DynamicType.Builderを返却する。
end note
```

処理の流れ

```plantuml
set separator off
Participant CombiningTransformerBuilder
Participant Instrumenter
Participant HelperScanner

Participant HelperTransformer
Participant FieldBackedContextRequestRewriter
Participant VisitingTransformer

collections adviceList <<AgentBuilder.TransformerのList>> 
activate adviceList

box Byte Buddy
    Participant AgentBuilder.Transformer.ForAdvice <<AgentBuilder.Transformer>> 
end box

Participant AdviceStack <<AgentBuilder.Transformer>> 
collections transformers <<AdviceStackを保持>>


activate CombiningTransformerBuilder
activate Instrumenter
CombiningTransformerBuilder -> CombiningTransformerBuilder : buildInstrumentationAdvice
activate CombiningTransformerBuilder


CombiningTransformerBuilder -> Instrumenter : helperClassNames()
alt HelperClassが何らかクラスに依存している場合
    CombiningTransformerBuilder -> HelperScanner : withClassDependencies(helperClassNames)
    activate HelperScanner
    HelperScanner --> CombiningTransformerBuilder : 依存クラスも含めた適切なロード順番のclassリスト
    deactivate HelperScanner
end 

CombiningTransformerBuilder -> HelperTransformer : new
activate HelperTransformer

CombiningTransformerBuilder -> adviceList : add

alt instrumenter.contextStore() を持っている場合
    CombiningTransformerBuilder -> FieldBackedContextRequestRewriter : new
    activate FieldBackedContextRequestRewriter
    CombiningTransformerBuilder ->  VisitingTransformer : new
    activate VisitingTransformer
    CombiningTransformerBuilder -> adviceList : add

    ref over CombiningTransformerBuilder
        registerContextStoreInjectionのシーケンス参照
    end ref 
end 

CombiningTransformerBuilder -> Instrumenter : transformer()
alt カスタムTransformerを持っている場合
    CombiningTransformerBuilder -> adviceList : add(customTransformer::transform)
end 

CombiningTransformerBuilder -> Instrumenter : methodIgnoreMatcher()
CombiningTransformerBuilder -> Instrumenter : adviceTransformations(this)
activate Instrumenter


== Transformer生成箇所==

loop 複数のパターンに対応する場合は、その分繰り返す。
    Instrumenter -> CombiningTransformerBuilder : applyAdvice(matcher,@AdviceをつけたクラスのFQDN)
    activate CombiningTransformerBuilder
    note right
        @Adviceは、ByteBuddyがメソッドの前後に追加処理を埋め込むためのアノテーション
        - @Advice.OnMethodEnter : メソッドが呼び出される前
        - @Advice.OnMethodExit  : メソッドが抜けた後

        matcherには、上記の処理を埋め込むメソッドを指定している。具体的には
        - メソッド名
        - 引数
        を指定している。

        なお、オーバーロードされたメソッドの場合は引数が異なるので、それぞれごとにmatcherを指定してループする。
        - ApacheHttpClientInstrumentationの場合は、8パターンあり。
    end note

    CombiningTransformerBuilder -> AgentBuilder.Transformer.ForAdvice : CustomMapping付きで生成
    activate AgentBuilder.Transformer.ForAdvice
    note right
        IgnoreListに当たらないもので、
        InsturemnterからもらったMatcherに合う条件のメソッドを
        Insturmenterが渡してきたメソッド名でAdviceを使って設定する。
    end note

    CombiningTransformerBuilder -> adviceList : add（生成したAgentBuilder.Transformer）
    deactivate CombiningTransformerBuilder
end loop

Instrumenter --> CombiningTransformerBuilder
deactivate Instrumenter

== ==
CombiningTransformerBuilder -> AdviceStack : new (adviceListを渡す)
activate AdviceStack
note over AdviceStack
    Transformerのリスト
end note

CombiningTransformerBuilder -> transformers : AdviceStack をセット
note over CombiningTransformerBuilder
    生成したTransformerは
    CombiningTransformerBuilderのprivateメンバー変数 AdviceStack[] transformersで管理される。
    この処理を抜けた後、AgengtInstaller側に戻ってから TransformerBuilder::installOn()が実行され、
    その中でActiveStackのTransformerを使って、「ClassFileTransformer」が生成される。
end note
CombiningTransformerBuilder -> adviceList : clear
```

```java
    @Override
    protected void buildInstrumentation(Instrumenter.Default instrumenter) {
        InstrumenterState.registerInstrumentation(instrumenter);
        int id = instrumenter.instrumentationId();
        if (transformers[id] != null) {
            // これは、実行時に設定される追加の "dd.trace.methods" インストゥルメンターです
            // (「dd.trace.methods」にリストされているクラスごとに個別のインスタンスが作成されます) 
            // トレースメソッドが混在しないように、マッチングのために個別のIDを割り当てます
            id = nextSupplementaryId++;
            if (transformers.length <= id) {
                transformers = Arrays.copyOf(transformers, id + 1);
            }
        }

        buildInstrumentationMatcher(instrumenter, id);
        buildInstrumentationAdvice(instrumenter, id);
    }

    @Override
    protected void buildSingleAdvice(Instrumenter.ForSingleType instrumenter) {

        // これは、動的IDを必要とするテストインストゥルメンターです
        int id = nextSupplementaryId++;
        if (transformers.length <= id) {
            transformers = Arrays.copyOf(transformers, id + 1);
        }

        // 既知の型インデックスはテストインストゥルメンターが含まれていないため使用できません
        matchers.add(new MatchRecorder.ForType(id, named(instrumenter.instrumentedType())));

        ignoredMethods = isSynthetic();
        ((Instrumenter.HasAdvice) instrumenter).adviceTransformations(this);
        transformers[id] = new AdviceStack(advice);

        advice.clear();
    }
```
