# Agent

## 概要

## シーケンス図

### Agent::startのシーケンス図

```plantuml
box 
    participant AgentBootstrap
end box 

box datadog.trace.bootstrap
    participant Agent
    participant InstallDatadogTracerCallback
end box

box datadog.trace.util
    participant AgentTaskScheduler
end box


AgentBootstrap -> AgentBootstrap : premain
activate AgentBootstrap

AgentBootstrap -> AgentBootstrap : agentmain
activate AgentBootstrap

AgentBootstrap -> AgentBootstrap : installAgentJar
activate AgentBootstrap

deactivate AgentBootstrap

AgentBootstrap -> Agent : start
activate Agent



Agent -> Agent : createAgentClassloader
activate Agent
note over Agent 
   NativeImageで実行されている場合は、
   startDatadogAgentを呼び出して終了する
end note

note over Agent
    - デフォルト有効
        - JMXFetch と Remote Config と Telemetry 
    - デフォルト無効
        iast,usm,appSec,appSecFully,cws(startCwsAgent),debugger
end note

Agent -> Agent : startProfilingAgent(true)
activate Agent 
note right
    スレッドを起こす
    具体的には、com.datadog.profiling.agent.ProfilingAgentのrunメソッドを呼び出す。
end note
deactivate Agent

Agent -> Agent :startCwsAgent
activate Agent
note right 
    詳細は確認する（デフォルト無効なので後回しにする）
end note
deactivate Agent

Agent -> AgentTaskScheduler : initialize
activate AgentTaskScheduler
note over AgentTaskScheduler
    AgentTaskSchedulerのスレッド名を初期化
end note


Agent -> Agent : startDatadogAgent(inst)
activate Agent
Agent -> AgentInstaller : installByteBuddyAgent
note over Agent, AgentInstaller
    **installByteBuddyAgentの概要**
    - ByteBuddyのAgentBuilderを生成して組み込んでいて、ここで関連するinsrumenterを組み込んでいると思われる。
    - 再度読みなおし中。
end note
deactivate Agent

ref over Agent
    scheduleJmxStart
end ref

note right Agent
    jmxFetch、またはprofilingが有効な場合、scheduleJmxStartを呼び出す
    デフォルト設定ではjmxFetchが有効なため実行される
end note

Agent -> InstallDatadogTracerCallback : new(instrumentation)
activate InstallDatadogTracerCallback
Agent -> InstallDatadogTracerCallback : execute()

ref over Agent
    Profilingが有効な場合
    startProfilingAgentのシーケンスを参照
end ref

```

### startDatadogAgentのシーケンス図

- 現在、このシーケンス＆AgentInstallerのシーケンスを作成中
- AgentInstaller は、dd-java-agent\agent-builder配下にある。

```plantuml
box datadog.trace.bootstrap
    participant Agent
end box 

box datadog.trace.agent.tooling
    participant AgentInstaller
    collections Instrumenter
    participant TransformerBuilder
end box

box datadog.trace.api 
    participant InstrumenterConfig
end box

box datadog.trace.util
    participant AgentTaskScheduler
end box

box net.bytebuddy
    participant ByteBuddy
end box

box  net.bytebuddy.agent.builder
    participant AgentBuilder.Default
end box


Agent -> Agent : startDatadogAgent
activate Agent

Agent -> AgentInstaller : installBytebuddyAgent
activate AgentInstaller

AgentInstaller -> AgentInstaller : getEnabledSystems
activate AgentInstaller
deactivate AgentInstaller

AgentInstaller -> AgentInstaller : installBytebuddyAgent \n 別引数のやつを呼び出す
activate AgentInstaller
note over AgentInstaller
    コンフィグの設定などここでやっている。
    詳細は別途確認する。
    Utils.setInstrumentation(inst) から
    ByteBuddyのインスタンス作成の直前まで
end note

note over AgentInstaller
    AgengtBuilderのインスタンスを生成
end note

AgentInstaller -> ByteBuddy : new
activate ByteBuddy

AgentInstaller -> AgentBuilder.Default : new(ByteBuddyのインスタンス)
activate AgentBuilder.Default

AgentInstaller -> Instrumenter : load
activate Instrumenter

AgentInstaller -> TransformerBuilder : new
note over AgentInstaller
    - InstrumenterConfigの設定次第で、いずれかで生成される
      - LegacyTransformerBuilder
      - CombiningTransformerBuilder
end note
activate TransformerBuilder

loop Instrumenter
    AgentInstaller -> Instrumenter : instrument(TransformerBuilder)
    note over Instrumenter
        - 引数として渡されるtransformerBuilderのapplyInstrumentation(this)を実行している。
        - この流れの場合、最終的に
           LegacyTransformerBuilder、CombiningTransformerBuilderのbuildInstrumentationを実行していることになる。
          (詳細はTransfomerBuilderの確認結果（別ファイル）にまとめる)
    end note
end
deactivate AgentInstaller

AgentInstaller -> AgentTaskScheduler : INSTANCE.scheduleAtFixedRate  内容は後で読む

```

### startProfilingAgentのシーケンス図

```plantuml

box datadog.trace.bootstrap
    participant Agent
end box


box datadog.trace.api
    participant WithGlobalTracer
end box


Agent -> Agent: startProfilingAgent(false) 引数のfalseは初回ではないことを指す
activate Agent

ref over Agent
    com.datadog.profiling.agent.ProfilingAgent.runを呼び出す
    詳細は別途確認する
end ref

Agent -> WithGlobalTracer : new
activate WithGlobalTracer
Agent -> WithGlobalTracer : Callback
activate WithGlobalTracer
note over WithGlobalTracer
    TracerAPIのregisterCheckpointerを**DatadogProfilerCheckpointer**
    TracerAPIのregisterTimerを**DatadogProfilerTimer**
    で呼び出すCallbackにする

    上記クラスのパッケージは**com.datadog.profiling.controller.ddprof**
end note 

deactivate Agent

ref over Agent
    InstrumentationBasedProfilingのenableInstrumentationBasedProfiling
    //内部実装 jfr有効であるとフラグを立てる
end ref
```


## ソースコード抜粋

```java

private static void startProfilingAgent(final boolean isStartingFirst) {
    
    //省略

    WithGlobalTracer.registerOrExecute(

        new WithGlobalTracer.Callback() 
        {

            @Override
            public void withTracer(TracerAPI tracer) {
                String checkpointerClassName = "com.datadog.profiling.controller.ddprof.DatadogProfilerCheckpointer"
                                                "com.datadog.profiling.controller.openjdk.JFRCheckpointer";
                String timerClassName =        "com.datadog.profiling.controller.ddprof.DatadogProfilerTimer"

                    // checkpointerClassNameのクラスを生成してtracer.registerCheckpointerを呼び出す
                    // timerClassNameのクラスを生成してtracer.registerCheckpointerを呼び出す
                    tracer.registerCheckpointer( 
                        // checkpointerClassNameを生成して呼び出す
                    );

                    tracer.registerTimer(
                        // timerClassNameを生成して呼び出す
                    );
            }
        }
    );

    // 省略
}

// 以下は datadog.trace.api.WithGlobalTracer のコピー

public class WithGlobalTracer {
    private static final Logger log = LoggerFactory.getLogger(WithGlobalTracer.class);

    private WithGlobalTracer() {}

    /**
     * Register a callback to be run when the global tracer is installed, or execute it right now if
     * the tracer is already installed.
     */
    public static void registerOrExecute(final Callback callback) {
        GlobalTracer.registerInstallationCallback(
            new GlobalTracer.Callback() {
            @Override
            public void installed(Tracer tracer) {
                if (tracer instanceof TracerAPI) {
                callback.withTracer((TracerAPI) tracer);
                } else {
                log.warn("Unsupported tracer type {}", tracer.getClass().getName());
                }
            }
            });
    }

    public interface Callback {
        void withTracer(TracerAPI tracer);
    }
}

```

```java
package datadog.trace.bootstrap.instrumentation.jfr;

public final class InstrumentationBasedProfiling {
  private static volatile boolean isJFRReady;

  public static void enableInstrumentationBasedProfiling() {
    isJFRReady = true;
  }

  public static boolean isJFRReady() {
    return isJFRReady;
  }
}
```

[Instrumenter](003_Instrumenter.md)
[TransfomerBuilder](004_TransfomerBuilder.md)

## 参考

### agent-bootstrap配下のソース構造

以下のディレクト配下の構造
dd-trace-java/dd-java-agent/agent-bootstrap/src/main/java/datadog/trace

ディレクトリのみ

```text
└── bootstrap
    ├── benchmark
    ├── blocking
    └── instrumentation
        ├── api
        ├── decorator
        │   └── http
        │       └── utils
        ├── httpurlconnection
        ├── java
        │   └── concurrent
        ├── jdbc
        ├── jfr
        ├── jms
        ├── messaging
        ├── rmi
        ├── shutdown
        ├── sslsocket
        └── usm
```

ファイル込み

```text
$ tree 
.
└── bootstrap
    ├── Agent.java                 　        AgentBootstrapから呼び出されるstartメソッドが定義されている
    ├── AgentClassLoading.java
    ├── AgentJarIndex.java
    ├── BootstrapProxy.java
    ├── CallDepthThreadLocalMap.java
    ├── Constants.java
    ├── ContextStore.java
    ├── DatadogClassLoader.java
    ├── ExceptionLogger.java
    ├── FieldBackedContextAccessor.java
    ├── FieldBackedContextStore.java
    ├── FieldBackedContextStores.java
    ├── InstanceStore.java
    ├── InstrumentationClassLoader.java
    ├── InstrumentationContext.java
    ├── Library.java
    ├── PatchLogger.java
    ├── WeakMap.java
    ├── WeakMapContextStore.java
    ├── benchmark
    │   └── StaticEventLogger.java
    ├── blocking
    │   └── BlockingActionHelper.java
    └── instrumentation
        ├── api
        │   └── ContextVisitors.java
        ├── decorator
        │   ├── AppSecUserEventDecorator.java
        │   ├── AsyncResultDecorator.java
        │   ├── BaseDecorator.java
        │   ├── ClientDecorator.java
        │   ├── DBTypeProcessingDatabaseClientDecorator.java
        │   ├── DatabaseClientDecorator.java
        │   ├── HttpClientDecorator.java
        │   ├── HttpServerDecorator.java
        │   ├── MessagingClientDecorator.java
        │   ├── OrmClientDecorator.java
        │   ├── ServerDecorator.java
        │   ├── UriBasedClientDecorator.java
        │   ├── UrlConnectionDecorator.java
        │   └── http
        │       ├── ClientIpAddressResolver.java
        │       ├── HttpResourceDecorator.java
        │       └── utils
        │           └── IPAddressUtil.java
        ├── httpurlconnection
        │   ├── HeadersInjectAdapter.java
        │   ├── HttpUrlConnectionDecorator.java
        │   └── HttpUrlState.java
        ├── java
        │   └── concurrent
        │       ├── AdviceUtils.java
        │       ├── ComparableRunnable.java
        │       ├── ConcurrentState.java
        │       ├── ContinuationClaim.java
        │       ├── ExcludeFilter.java
        │       ├── ExecutorInstrumentationUtils.java
        │       ├── NewTaskForPlaceholder.java
        │       ├── QueueTimerHelper.java
        │       ├── RunnableWrapper.java
        │       ├── State.java
        │       ├── TPEHelper.java
        │       └── Wrapper.java
        ├── jdbc
        │   ├── DBInfo.java
        │   ├── DBQueryInfo.java
        │   └── JDBCConnectionUrlParser.java
        ├── jfr
        │   └── InstrumentationBasedProfiling.java
        ├── jms
        │   ├── MessageBatchState.java
        │   ├── MessageConsumerState.java
        │   ├── MessageProducerState.java
        │   ├── SessionState.java
        │   └── TimeInQueue.java
        ├── messaging
        │   └── DatadogAttributeParser.java
        ├── rmi
        │   ├── ContextDispatcher.java
        │   ├── ContextPayload.java
        │   ├── ContextPropagator.java
        │   ├── RmiClientDecorator.java
        │   ├── RmiServerDecorator.java
        │   └── ThreadLocalContext.java
        ├── shutdown
        │   └── ShutdownHelper.java
        ├── sslsocket
        │   ├── UsmFilterInputStream.java
        │   └── UsmFilterOutputStream.java
        └── usm
            ├── UsmConnection.java
            ├── UsmExtractor.java
            ├── UsmMessage.java
            └── UsmMessageFactory.java

```
