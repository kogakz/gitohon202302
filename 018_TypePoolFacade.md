# TypePoolFacade

## 概要

## クラス図

```plantuml

set separator none
package net.bytebuddy.pool {
    interface TypePool
}

package datadog.trace.agent.tooling.bytebuddy {
    interface SharedTypePools.Supplier
}

package datadog.trace.agent.tooling.bytebuddy.outline{
    class TypePoolFacade implements  TypePool, SharedTypePools.Supplier {
         + static void registerAsSupplier()
    }
} 



```

## シーケンス図

### TypePoolFacade::registerAsSupplier

```plantuml
box datadog.trace.agent.tooling
    participant AgentInstaller
end box


box datadog.trace.agent.tooling.bytebuddy.outline
    participant TypePoolFacade
end box

box datadog.trace.agent.tooling.bytebuddy
    participant SharedTypePools
end box

box datadog.trace.agent.tooling.bytebuddy.outline
    participant TypeFactory.typeFactory as typeFactory << ThreadLocal<TypeFactory>>>
end box



activate AgentInstaller
activate typeFactory

AgentInstaller -> AgentInstaller : installBytebuddyAgent \n 別引数のやつを呼び出す
activate AgentInstaller

AgentInstaller -> TypePoolFacade : registerAsSupplier
note right
    1/17確認中
end note
activate TypePoolFacade

TypePoolFacade -> SharedTypePools : registerIfAbsent
note over TypePoolFacade, SharedTypePools
    TypePoolFacadeのインスタンスをSharedTypePoolsに登録する
    ※TypePoolFacadeをシングルトンインスタンスとして扱えるようになる。
      SharedTypePoolsに登録されるのは最初の1つだけ。
end note 

TypePoolFacade -> typeFactory : beginInstall()
activate typeFactory
note over typeFactory
    メンバー変数 installingをtrueにするだけ
end note





```


## ソースコード
