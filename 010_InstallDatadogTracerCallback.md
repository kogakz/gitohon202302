# InstallDatadogTracerCallback

## 概要

- Agent.java内で定義されている。

## シーケンス図

Agent::startメソッド内から実行されている
AgentTracer、GlobalTracerにCoreTracerを生成して登録する。

```plantuml
box datadog.trace.bootstrap
    participant Agent
    participant InstallDatadogTracerCallback
end box 

box datadog.communication.ddagent
    participant SharedCommunicationObjects
end box

box datadog.trace.agent.tooling
    participant TracerInstaller
end box

box datadog.telemetry
    participant TelemetrySystem
end box 

activate Agent

Agent -> InstallDatadogTracerCallback : new(Instrumentation)
activate InstallDatadogTracerCallback

Agent -> InstallDatadogTracerCallback : execute
activate InstallDatadogTracerCallback

note over InstallDatadogTracerCallback
    todo : startTelemetryを確認して加筆
end note

InstallDatadogTracerCallback -> SharedCommunicationObjects : new
activate SharedCommunicationObjects

InstallDatadogTracerCallback -> InstallDatadogTracerCallback : installDatadogTracer
activate InstallDatadogTracerCallback
InstallDatadogTracerCallback -> TracerInstaller : installGlobalTracer
note right
    AgentTracer、GlobalTracerにCoreTracerを生成して登録する。
end note 

InstallDatadogTracerCallback -> InstallDatadogTracerCallback : maybeStartAppSec
activate InstallDatadogTracerCallback
note right
    割愛
end note
deactivate InstallDatadogTracerCallback

InstallDatadogTracerCallback -> InstallDatadogTracerCallback : maybeStartIast
activate InstallDatadogTracerCallback
note right
    割愛
end note
deactivate InstallDatadogTracerCallback

InstallDatadogTracerCallback -> InstallDatadogTracerCallback : maybeStartCiVisibility
activate InstallDatadogTracerCallback
note right
    割愛
end note
deactivate InstallDatadogTracerCallback

InstallDatadogTracerCallback -> InstallDatadogTracerCallback : maybeStartDebugger
activate InstallDatadogTracerCallback
note right
    割愛
end note
deactivate InstallDatadogTracerCallback

InstallDatadogTracerCallback -> InstallDatadogTracerCallback : maybeStartRemoteConfig
activate InstallDatadogTracerCallback
note right
    割愛
end note
deactivate InstallDatadogTracerCallback

InstallDatadogTracerCallback -> InstallDatadogTracerCallback : startTelemetry
activate InstallDatadogTracerCallback
InstallDatadogTracerCallback -> TelemetrySystem : startTelemetry
note right
    todo : startTelemetryについて確認する
end note
deactivate InstallDatadogTracerCallback

```


## ソース

```java
protected static class InstallDatadogTracerCallback extends ClassLoadCallBack {
    private final Instrumentation instrumentation;

    public InstallDatadogTracerCallback(Instrumentation instrumentation) {
      this.instrumentation = instrumentation;
    }

    @Override
    public AgentThread agentThread() {
      return TRACE_STARTUP;
    }

    @Override
    public void execute() {
      Object sco;
      Class<?> scoClass;
      try {
        scoClass =
            AGENT_CLASSLOADER.loadClass("datadog.communication.ddagent.SharedCommunicationObjects");
        sco = scoClass.getConstructor().newInstance();
      } catch (ClassNotFoundException
          | NoSuchMethodException
          | InstantiationException
          | IllegalAccessException
          | InvocationTargetException e) {
        throw new UndeclaredThrowableException(e);
      }

      installDatadogTracer(scoClass, sco);
      maybeStartAppSec(scoClass, sco);
      maybeStartIast(scoClass, sco);
      maybeStartCiVisibility(scoClass, sco);
      // start debugger before remote config to subscribe to it before starting to poll
      maybeStartDebugger(instrumentation, scoClass, sco);
      maybeStartRemoteConfig(scoClass, sco);

      if (telemetryEnabled) {
        startTelemetry(instrumentation, scoClass, sco);
      }
    }
  
```