# AgentTaskScheduler

## initialize

AgentTaskSchedulerのスレッド名を設定する。
現在のスレッド名は"dd-task-scheduler"

実際のスレッド実行は別タイミングで行われている。

## AgentInstaller

この辺りは今の所有益な情報なし。

```plantuml
box datadog.trace.agent.tooling
    participant AgentInstaller
end box

note over AgentInstaller
    static定義で以下の初期化関数を呼び出している
    static {
        addByteBuddyRawSetting();
        // register weak map supplier as early as possible
        WeakMaps.registerAsSupplier();
        circularityErrorWorkaround();
    }
end note

AgentInstaller -> AgentInstaller : addByteBuddyRawSetting

AgentInstaller -> AgentInstaller : WeakMaps.registerAsSupplier

AgentInstaller -> AgentInstaller : circularityErrorWorkaround
```

