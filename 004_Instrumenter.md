# Instrumenter

## Instrumenter.instrumentの概要

InstrumenterのDefaultクラスのinstrumentmメソッドは`Instrumenter.Default`の`instrument`メソッドの実装では
自身が有効であれば、

```java
transformerBuilder.applyInstrumentation(this);
```

を呼び出すことになっている。

## Instrumenterのクラス図

```plantuml
skinparam groupInheritance 2
Interface Instrumenter {
  boolean isApplicable(Set<TargetSystem> enabledSystems)
  void instrument(TransformerBuilder transformerBuilder)
}
abstract Instrumenter_Default {
  + int instrumentationId() 
  + String name() 
  + Iterable<String> names()
  + void instrument(TransformerBuilder)
}

abstract Tracing
abstract Profiling
abstract AppSec
abstract Iast
abstract Usm
abstract CiVisibility
abstract CallSiteInstrumentation
class CalleeBenchmarkInstrumentation
class CallSiteBenchmarkInstrumentation

Instrumenter <|--- Instrumenter_Default
Instrumenter_Default <|-- Tracing
Instrumenter_Default <|-- Profiling
Instrumenter_Default <|-- AppSec
Instrumenter_Default <|-- Iast
Instrumenter_Default <|-- Usm
Instrumenter_Default <|-- CiVisibility
Instrumenter_Default <|-- CalleeBenchmarkInstrumentation
Instrumenter_Default <|-- CallSiteInstrumentation
CallSiteInstrumentation <|-- CallSiteBenchmarkInstrumentation

interface HasAdvice {
  void adviceTransformations(AdviceTransformation transformation);
}

Instrumenter  +-- HasAdvice

note right of Instrumenter_Default::instrument(TransformerBuilder)
  transformerBuilder.applyInstrumentation(this)を実行する
end note 

```

AgentInstaller内で以下のMETA-INFに定義されているクラス名を読み込んで
instrument(transformerBuilder)を実行している。

```text
│   ├── agent-tooling
│   │   ├── build.gradle
│   │   └── src
│   │       ├── jmh
│   │       │   └── resources
│   │       │       └── META-INF
│   │       │           └── services
│   │       │               └── datadog.trace.agent.tooling.Instrumenter
```

定義されているクラス名

```text
datadog.trace.agent.tooling.bytebuddy.csi.CalleeBenchmarkInstrumentation
datadog.trace.agent.tooling.bytebuddy.csi.CallSiteBenchmarkInstrumentation
```

- 補足
  - IAST (Interactive Application Security Testing)
  - AppSec(Datadog Application Security Management)
  - Tracing(APMが有効な時)
  - Profiling(Continuous Profilerが有効な時）
  - USM(Universal Service Monitoringが有効な時)
  - CiVisibility(Continuous Integration Visibilityが有効な時）

## Instrumenter.Defaultクラス
