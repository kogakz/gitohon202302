# TraceInterceptorのまとめ

## 概要

### クラス図

省略

## コンストラクタ

どのクラスもsuperクラス（AbstractTraceInterceptor）のコンストラクタを呼び出しているだけで、priorityをセットしているだけ。

- CI_VISIBILITY_TRACE(0) : CiVisibilityTraceInterceptor
- CI_VISIBILITY_APM(1) : CiVisibilityApmProtocolInterceptor
- DD_INTAKE(2) : DDIntakeTraceInterceptor
- GIT_METADATA(3) : GitMetadataTraceInterceptor

### 各クラスのコンストラクタのソース

```java
  protected AbstractTraceInterceptor(Priority priority) {
    this.priority = priority;
  }
```

```java
  protected CiVisibilityTraceInterceptor(Priority priority) {
    super(priority);
  }

  protected DDIntakeTraceInterceptor(Priority priority) {
    super(priority);
  }

  protected CiVisibilityApmProtocolInterceptor(Priority priority) {
    super(priority);
  }

  protected GitMetadataTraceInterceptor(Priority priority) {
    super(priority);
  }
```

## ソース

### AbstractTraceInterceptorのソース

```java
package datadog.trace.api.interceptor;

public abstract class AbstractTraceInterceptor implements TraceInterceptor {

  private final Priority priority;

  protected AbstractTraceInterceptor(Priority priority) {
    this.priority = priority;
  }

  @Override
  public int priority() {
    return priority.idx;
  }

  public enum Priority {
    // trace filtering
    CI_VISIBILITY_TRACE(0),
    CI_VISIBILITY_APM(1),

    // trace data enrichment
    DD_INTAKE(2),
    GIT_METADATA(3),

    // trace data collection
    SERVICE_NAME_COLLECTING(Integer.MAX_VALUE);

    private final int idx;

    Priority(int idx) {
      this.idx = idx;
    }

    int getIdx() {
      return idx;
    }
  }
}

```

### CiVisibilityTraceInterceptorのソース

```java
package datadog.trace.civisibility.interceptor;

import datadog.trace.api.DDSpanTypes;
import datadog.trace.api.DDTags;
import datadog.trace.api.interceptor.AbstractTraceInterceptor;
import datadog.trace.api.interceptor.MutableSpan;
import datadog.trace.bootstrap.instrumentation.api.UTF8BytesString;
import datadog.trace.core.DDSpan;
import datadog.trace.core.DDTraceCoreInfo;
import java.util.Collection;
import java.util.Collections;

public class CiVisibilityTraceInterceptor extends AbstractTraceInterceptor {

  public static final CiVisibilityTraceInterceptor INSTANCE =
      new CiVisibilityTraceInterceptor(Priority.CI_VISIBILITY_TRACE);

  static final UTF8BytesString CIAPP_TEST_ORIGIN = UTF8BytesString.create("ciapp-test");

  protected CiVisibilityTraceInterceptor(Priority priority) {
    super(priority);
  }

  @Override
  public Collection<? extends MutableSpan> onTraceComplete(
      Collection<? extends MutableSpan> trace) {
    if (trace.isEmpty()) {
      return trace;
    }

    final DDSpan firstSpan = (DDSpan) trace.iterator().next();
    final DDSpan localRootSpan = firstSpan.getLocalRootSpan();

    final DDSpan spanToCheck = null == localRootSpan ? firstSpan : localRootSpan;

    // If root span is not a CI visibility span, we drop the full trace.
    CharSequence type = spanToCheck.getType(); // Don't null pointer if there is no type
    if (type == null
        || (!DDSpanTypes.TEST.contentEquals(type)
            && !DDSpanTypes.TEST_SUITE_END.contentEquals(type)
            && !DDSpanTypes.TEST_MODULE_END.contentEquals(type)
            && !DDSpanTypes.TEST_SESSION_END.contentEquals(type))) {
      return Collections.emptyList();
    }

    // If the trace belongs to a "test", we need to set the origin to `ciapp-test` and the
    // `library_version` tag for all spans.
    firstSpan.context().setOrigin(CIAPP_TEST_ORIGIN);
    firstSpan.setTag(DDTags.LIBRARY_VERSION_TAG_KEY, DDTraceCoreInfo.VERSION);
    for (MutableSpan span : trace) {
      span.setTag(DDTags.LIBRARY_VERSION_TAG_KEY, DDTraceCoreInfo.VERSION);
    }

    return trace;
  }
}

```

### DDIntakeTraceInterceptorのソース

```java
package datadog.trace.common.writer.ddintake;

import static datadog.trace.util.TraceUtils.isValidStatusCode;
import static datadog.trace.util.TraceUtils.normalizeOperationName;
import static datadog.trace.util.TraceUtils.normalizeServiceName;
import static datadog.trace.util.TraceUtils.normalizeSpanType;

import datadog.trace.api.interceptor.AbstractTraceInterceptor;
import datadog.trace.api.interceptor.MutableSpan;
import datadog.trace.bootstrap.instrumentation.api.Tags;
import datadog.trace.core.DDSpan;
import datadog.trace.util.TraceUtils;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DDIntakeTraceInterceptor extends AbstractTraceInterceptor {

  public static final DDIntakeTraceInterceptor INSTANCE =
      new DDIntakeTraceInterceptor(Priority.DD_INTAKE);

  private static final Logger log = LoggerFactory.getLogger(DDIntakeTraceInterceptor.class);

  protected DDIntakeTraceInterceptor(Priority priority) {
    super(priority);
  }

  @Override
  public Collection<? extends MutableSpan> onTraceComplete(
      Collection<? extends MutableSpan> trace) {
    if (trace.isEmpty()) {
      return trace;
    }

    for (MutableSpan span : trace) {
      if (span instanceof DDSpan) {
        process((DDSpan) span);
      }
    }
    return trace;
  }

  private void process(DDSpan span) {
    span.setServiceName(normalizeServiceName(span.getServiceName()));
    span.setOperationName(normalizeOperationName(span.getOperationName()));
    span.setSpanType(normalizeSpanType(span.getType()));

    if (span.getResourceName() == null || span.getResourceName().length() == 0) {
      log.debug(
          "Fixing malformed trace. Resource is empty (reason:resource_empty), setting span.resource={}: {}",
          span.getOperationName(),
          span);
      span.setResourceName(span.getOperationName());
    }

    span.setTag(Tags.ENV, TraceUtils.normalizeEnv((String) span.getTag(Tags.ENV)));

    final short httpStatusCode = span.getHttpStatusCode();
    if (httpStatusCode != 0 && !isValidStatusCode(httpStatusCode)) {
      log.debug(
          "Fixing malformed trace. HTTP status code is invalid (reason:invalid_http_status_code), dropping invalid http.status_code={}: {}",
          httpStatusCode,
          span);
      span.setHttpStatusCode(0);
    }
  }
}

```

### GitMetadataTraceInterceptorのソース

```java
package datadog.trace.common;

import datadog.trace.api.DDTags;
import datadog.trace.api.git.GitInfo;
import datadog.trace.api.git.GitInfoProvider;
import datadog.trace.api.interceptor.AbstractTraceInterceptor;
import datadog.trace.api.interceptor.MutableSpan;
import datadog.trace.api.interceptor.TraceInterceptor;
import datadog.trace.bootstrap.instrumentation.api.Tags;
import datadog.trace.core.DDSpan;
import java.util.Collection;

public class GitMetadataTraceInterceptor extends AbstractTraceInterceptor {

  public static final TraceInterceptor INSTANCE =
      new GitMetadataTraceInterceptor(Priority.GIT_METADATA);

  protected GitMetadataTraceInterceptor(Priority priority) {
    super(priority);
  }

  @Override
  public Collection<? extends MutableSpan> onTraceComplete(
      Collection<? extends MutableSpan> trace) {
    if (trace.isEmpty()) {
      return trace;
    }

    final DDSpan firstSpan = (DDSpan) trace.iterator().next();
    String ciWorkspacePath = (String) firstSpan.getTag(Tags.CI_WORKSPACE_PATH);

    GitInfo gitInfo = GitInfoProvider.INSTANCE.getGitInfo(ciWorkspacePath);
    firstSpan.setTag(DDTags.INTERNAL_GIT_REPOSITORY_URL, gitInfo.getRepositoryURL());
    firstSpan.setTag(DDTags.INTERNAL_GIT_COMMIT_SHA, gitInfo.getCommit().getSha());

    return trace;
  }
}

```
