# AgentInstaller

## 概要

premainからの流れで呼び出されてくる。
ByteBuddy

## シーケンス図

### AgentInstaller::installByteBuddyAgentのシーケンス

#### AgentInstaller::installByteBuddyAgentの初期化部分

- 現在、このシーケンス＆AgentInstallerのシーケンスを作成中
- AgentInstaller は、dd-java-agent\agent-builder配下にある。

```plantuml
box datadog.trace.bootstrap
    participant Agent
end box 

box datadog.trace.agent.tooling
    participant AgentInstaller
    participant Utils
end box

box datadog.trace.util
    participant AgentTaskScheduler
end box

box net.bytebuddy
    participant ByteBuddy
end box

box  net.bytebuddy.agent.builder
    participant AgentBuilder.Default
end box


Agent -> Agent : startDatadogAgent
activate Agent

Agent -> AgentInstaller : installBytebuddyAgent
activate AgentInstaller

AgentInstaller -> AgentInstaller : getEnabledSystems
activate AgentInstaller
deactivate AgentInstaller

AgentInstaller -> AgentInstaller : installBytebuddyAgent \n 別引数のやつを呼び出す
activate AgentInstaller

== 初期設定 == 

AgentInstaller -> Utils : setInstrumentation
note over AgentInstaller , Utils
    最初にセットされたinstrumentationが必ず使われるように
    AtomicReference<Instrumentation> としてセットする。
    以降は、Utils.getgetInstrumentation()でinstrumentationを取得できる。
end note

ref over AgentInstaller
  TypePoolFacade::registerAsSupplierを呼び出して
  TypePoolFacedクラスをSharedTypePoolsに登録する。
  またスレッドローカルのTypePoolFacade変数にinstall開始フラグを立てる。
  018_TypePoolFacade.mdを参照
end

ref over AgentInstaller
  System.propertyや、環境変数から設定項目を読み込む。（InstrumenterConfig.javaを参照）
  設定項目名は、datadog.trace.api.config配下のTraceInstrumentationConfigなどに定義されている。
  ByteBuddyのインスタンス作成の直前まで
end ref

ref over AgentInstaller
  USM(Universal Service Monitoring）が有効な場合、以下を呼び出す。（いったん飛ばす）
  - UsmMessageFactoryImpl.registerAsSupplier();
  - UsmExtractorImpl.registerAsSupplier();
end ref


note over AgentInstaller
    AgengtBuilderのインスタンスを生成
end note

AgentInstaller -> ByteBuddy : new
activate ByteBuddy
AgentInstaller -> AgentBuilder.Default : new(ByteBuddyのインスタンス)
activate AgentBuilder.Default

== Instrumenter生成＆TransformerBuilderへのセット == 
ref over AgentInstaller
  Instrumenter生成＆TransformerBuilderへのセット箇所を参照
end ref
== == 

AgentInstaller -> AgentTaskScheduler : INSTANCE.scheduleAtFixedRate  内容は後で読む

```

#### Instrumenter生成＆TransformerBuilderへのセット

```plantuml


box datadog.trace.agent.tooling
    participant AgentInstaller
    collections Instrumenters 
    collections InstrumenterState
    participant Instrumenter
    collections ExcludeFilter
    participant TransformerBuilder
    participant InstrumenterState.Observer
    
end box

activate AgentInstaller

AgentInstaller -> Instrumenters : load
note right
  Instrumenterを実装したクラスの名前を
  META-INF/services/datadog.trace.agent.tooling.Instrument
  から読み込む。
  
  Instrumenters::loadを参照

  
end note
activate Instrumenters

AgentInstaller -> InstrumenterState : setMaxInstrumentationId
note over InstrumenterState
  ステータスを保持できるよう
  Instrumentersのサイズに合わせる
end note
activate InstrumenterState

== Instrumenterの生成 ==
loop Instrumentersで管理しているInstrumenterのクラスパスごとに実施
  Instrumenters -> Instrumenter : new
  activate Instrumenter
  note over Instrumenters, Instrumenter
    対象のInstrumenterが未生成なら生成してInstrumenters内で保持する。
    生成済みであればそのインスタンスを返却する
    ※ここでは初回呼び出しなので生成するルート
  end note
  alt InstrumentersにロードされたInstrumenterが\nExcludeFilterを実装している場合
    AgentInstaller -> ExcludeFilter : add
    activate ExcludeFilter
    note over ExcludeFilter
      InstrumenterがExcludeFilterも実装している場合
      対象のインスタンスをExcludeFilterに登録する
    end note
  end
end

== TransformBuilderへの登録 ==

AgentInstaller -> TransformerBuilder : new
note over TransformerBuilder
    - InstrumenterConfigの設定次第で、いずれかで生成される
      - LegacyTransformerBuilder
      - CombiningTransformerBuilder
end note
activate TransformerBuilder

loop Instrumenters
    AgentInstaller -> Instrumenter : instrument(TransformerBuilder)
    note over Instrumenter
        ここでは結果をまとめる。
        - Instrumenter::insturmentを呼び出すと自身が有効な場合は、引数として渡されるtransformerBuilderのapplyInstrumentation(this)を実行している。
          - 補足 ここで実行されているInstrumenterのインスタンスは、`ApacheHttpClientInstrumentation`など個別クラス
        - この流れの場合、最終的に
           LegacyTransformerBuilder、CombiningTransformerBuilderのbuildInstrumentationを実行していることになる。
          
    end note

    alt InstrumenterConfig.get().isTelemetryEnabled()
      AgentInstaller -> InstrumenterState.Observer : new
      activate InstrumenterState.Observer
      AgentInstaller -> InstrumenterState : setObserver
    end

    AgentInstaller -> InstrumenterState : resetDefaultState()
    
    AgentInstaller -> TransformerBuilder : installOn()

end
deactivate AgentInstaller


```


## ソース

### AgentInstallerのソース

dd-java-agent/agent-builder/src/main/java/datadog/trace/agent/tooling/AgentInstaller.java

```java
package datadog.trace.agent.tooling;

import static datadog.trace.agent.tooling.bytebuddy.matcher.GlobalIgnoresMatcher.globalIgnoresMatcher;
import static net.bytebuddy.matcher.ElementMatchers.isDefaultFinalizer;

import datadog.trace.agent.tooling.bytebuddy.SharedTypePools;
import datadog.trace.agent.tooling.bytebuddy.iast.TaintableRedefinitionStrategyListener;
import datadog.trace.agent.tooling.bytebuddy.matcher.DDElementMatchers;
import datadog.trace.agent.tooling.bytebuddy.memoize.MemoizedMatchers;
import datadog.trace.agent.tooling.bytebuddy.outline.TypePoolFacade;
import datadog.trace.agent.tooling.usm.UsmExtractorImpl;
import datadog.trace.agent.tooling.usm.UsmMessageFactoryImpl;
import datadog.trace.api.InstrumenterConfig;
import datadog.trace.api.ProductActivation;
import datadog.trace.api.telemetry.IntegrationsCollector;
import datadog.trace.bootstrap.FieldBackedContextAccessor;
import datadog.trace.bootstrap.instrumentation.java.concurrent.ExcludeFilter;
import datadog.trace.util.AgentTaskScheduler;
import de.thetaphi.forbiddenapis.SuppressForbidden;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.agent.builder.AgentBuilder;
import net.bytebuddy.description.type.TypeDefinition;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.matcher.LatentMatcher;
import net.bytebuddy.utility.JavaModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AgentInstaller {
  private static final Logger log = LoggerFactory.getLogger(AgentInstaller.class);
  private static final boolean DEBUG = log.isDebugEnabled();

  private static final List<Runnable> LOG_MANAGER_CALLBACKS = new CopyOnWriteArrayList<>();
  private static final List<Runnable> MBEAN_SERVER_BUILDER_CALLBACKS = new CopyOnWriteArrayList<>();

  static {
    addByteBuddyRawSetting();
    // register weak map supplier as early as possible
    WeakMaps.registerAsSupplier();
    circularityErrorWorkaround();
  }

  @SuppressForbidden
  private static void circularityErrorWorkaround() {
    // these classes have been involved in intermittent ClassCircularityErrors during startup
    // they don't need context storage, so it's safe to load them before installing the agent
    try {
      Class.forName("java.util.concurrent.ThreadLocalRandom");
    } catch (Throwable ignore) {
    }
  }

  public static void installBytebuddyAgent(final Instrumentation inst) {
    /*
     * ByteBuddy agent is used by several systems which can be enabled independently;
     * we need to install the agent whenever any of them is active.
     */
    Set<Instrumenter.TargetSystem> enabledSystems = getEnabledSystems();
    if (!enabledSystems.isEmpty()) {
      installBytebuddyAgent(inst, false, enabledSystems);
      if (DEBUG) {
        log.debug("Instrumentation installed for {}", enabledSystems);
      }
      int poolCleaningInterval = InstrumenterConfig.get().getResolverResetInterval();
      if (poolCleaningInterval > 0) {
        AgentTaskScheduler.INSTANCE.scheduleAtFixedRate(
            SharedTypePools::clear,
            poolCleaningInterval,
            Math.max(poolCleaningInterval, 10),
            TimeUnit.SECONDS);
      }
    } else if (DEBUG) {
      log.debug("No target systems enabled, skipping instrumentation.");
    }
  }

  /**
   * Install the core bytebuddy agent along with all implementations of {@link Instrumenter}.
   *
   * @return the agent's class transformer
   */
  public static ClassFileTransformer installBytebuddyAgent(
      final Instrumentation inst,
      final boolean skipAdditionalLibraryMatcher,
      final Set<Instrumenter.TargetSystem> enabledSystems,
      final AgentBuilder.Listener... listeners) {
    Utils.setInstrumentation(inst);

    TypePoolFacade.registerAsSupplier();

    if (InstrumenterConfig.get().isResolverMemoizingEnabled()) {
      MemoizedMatchers.registerAsSupplier();
    } else {
      DDElementMatchers.registerAsSupplier();
    }

    if (enabledSystems.contains(Instrumenter.TargetSystem.USM)) {
      UsmMessageFactoryImpl.registerAsSupplier();
      UsmExtractorImpl.registerAsSupplier();
    }

    // By default ByteBuddy will skip all methods that are synthetic or default finalizer
    // but we need to instrument some synthetic methods in Scala, so change the ignore matcher
    ByteBuddy byteBuddy =
        new ByteBuddy().ignore(new LatentMatcher.Resolved<>(isDefaultFinalizer()));
    AgentBuilder agentBuilder =
        new AgentBuilder.Default(byteBuddy)
            .disableClassFormatChanges()
            .assureReadEdgeTo(inst, FieldBackedContextAccessor.class)
            .with(AgentStrategies.transformerDecorator())
            .with(AgentBuilder.RedefinitionStrategy.RETRANSFORMATION)
            .with(AgentStrategies.rediscoveryStrategy())
            .with(redefinitionStrategyListener(enabledSystems))
            .with(AgentStrategies.locationStrategy())
            .with(AgentStrategies.poolStrategy())
            .with(AgentBuilder.DescriptionStrategy.Default.POOL_ONLY)
            .with(AgentStrategies.bufferStrategy())
            .with(AgentStrategies.typeStrategy())
            .with(new ClassLoadListener())
            // FIXME: we cannot enable it yet due to BB/JVM bug, see
            // https://github.com/raphw/byte-buddy/issues/558
            // .with(AgentBuilder.LambdaInstrumentationStrategy.ENABLED)
            .ignore(globalIgnoresMatcher(skipAdditionalLibraryMatcher));

    if (DEBUG) {
      agentBuilder =
          agentBuilder
              .with(AgentBuilder.RedefinitionStrategy.RETRANSFORMATION)
              .with(AgentStrategies.rediscoveryStrategy())
              .with(redefinitionStrategyListener(enabledSystems))
              .with(new RedefinitionLoggingListener())
              .with(new TransformLoggingListener());
    }

    for (final AgentBuilder.Listener listener : listeners) {
      agentBuilder = agentBuilder.with(listener);
    }

    Instrumenters instrumenters = Instrumenters.load(AgentInstaller.class.getClassLoader());
    int maxInstrumentationId = instrumenters.maxInstrumentationId();

    // pre-size state before registering instrumentations to reduce number of allocations
    InstrumenterState.setMaxInstrumentationId(maxInstrumentationId);

    // This needs to be a separate loop through all the instrumenters before we start adding
    // advice so that we can exclude field injection, since that will try to check exclusion
    // immediately and we don't have the ability to express dependencies between different
    // instrumenters to control the load order.
    for (Instrumenter instrumenter : instrumenters) {
      if (instrumenter instanceof ExcludeFilterProvider) {
        ExcludeFilterProvider provider = (ExcludeFilterProvider) instrumenter;
        ExcludeFilter.add(provider.excludedClasses());
        if (DEBUG) {
          log.debug(
              "Adding filtered classes - instrumentation.class={}",
              instrumenter.getClass().getName());
        }
      }
    }

    Instrumenter.TransformerBuilder transformerBuilder;
    if (InstrumenterConfig.get().isLegacyInstallerEnabled()) {
      transformerBuilder = new LegacyTransformerBuilder(agentBuilder);
    } else {
      transformerBuilder = new CombiningTransformerBuilder(agentBuilder, maxInstrumentationId);
    }

    int installedCount = 0;
    for (Instrumenter instrumenter : instrumenters) {
      if (!instrumenter.isApplicable(enabledSystems)) {
        if (DEBUG) {
          log.debug("Not applicable - instrumentation.class={}", instrumenter.getClass().getName());
        }
        continue;
      }
      if (DEBUG) {
        log.debug("Loading - instrumentation.class={}", instrumenter.getClass().getName());
      }
      try {
        instrumenter.instrument(transformerBuilder);
        installedCount++;
      } catch (Exception | LinkageError e) {
        log.error(
            "Failed to load - instrumentation.class={}", instrumenter.getClass().getName(), e);
      }
    }
    if (DEBUG) {
      log.debug("Installed {} instrumenter(s)", installedCount);
    }

    if (InstrumenterConfig.get().isTelemetryEnabled()) {
      InstrumenterState.setObserver(
          new InstrumenterState.Observer() {
            @Override
            public void applied(Iterable<String> instrumentationNames) {
              IntegrationsCollector.get().update(instrumentationNames, true);
            }
          });
    }

    InstrumenterState.resetDefaultState();
    try {
      return transformerBuilder.installOn(inst);
    } finally {
      SharedTypePools.endInstall();
    }
  }

  public static Set<Instrumenter.TargetSystem> getEnabledSystems() {
    EnumSet<Instrumenter.TargetSystem> enabledSystems =
        EnumSet.noneOf(Instrumenter.TargetSystem.class);
    InstrumenterConfig cfg = InstrumenterConfig.get();
    if (cfg.isTraceEnabled()) {
      enabledSystems.add(Instrumenter.TargetSystem.TRACING);
    }
    if (cfg.isProfilingEnabled()) {
      enabledSystems.add(Instrumenter.TargetSystem.PROFILING);
    }
    if (cfg.getAppSecActivation() != ProductActivation.FULLY_DISABLED) {
      enabledSystems.add(Instrumenter.TargetSystem.APPSEC);
    }
    if (cfg.getIastActivation() != ProductActivation.FULLY_DISABLED) {
      enabledSystems.add(Instrumenter.TargetSystem.IAST);
    }
    if (cfg.isCiVisibilityEnabled()) {
      enabledSystems.add(Instrumenter.TargetSystem.CIVISIBILITY);
    }
    if (cfg.isUsmEnabled()) {
      enabledSystems.add(Instrumenter.TargetSystem.USM);
    }
    return enabledSystems;
  }

  private static void addByteBuddyRawSetting() {
    final String savedPropertyValue = System.getProperty(TypeDefinition.RAW_TYPES_PROPERTY);
    try {
      System.setProperty(TypeDefinition.RAW_TYPES_PROPERTY, "true");
      final boolean rawTypes = TypeDescription.AbstractBase.RAW_TYPES;
      if (!rawTypes && DEBUG) {
        log.debug("Too late to enable {}", TypeDefinition.RAW_TYPES_PROPERTY);
      }
    } finally {
      if (savedPropertyValue == null) {
        System.clearProperty(TypeDefinition.RAW_TYPES_PROPERTY);
      } else {
        System.setProperty(TypeDefinition.RAW_TYPES_PROPERTY, savedPropertyValue);
      }
    }
  }

  private static AgentBuilder.RedefinitionStrategy.Listener redefinitionStrategyListener(
      final Set<Instrumenter.TargetSystem> enabledSystems) {
    if (enabledSystems.contains(Instrumenter.TargetSystem.IAST)) {
      return TaintableRedefinitionStrategyListener.INSTANCE;
    } else {
      return AgentBuilder.RedefinitionStrategy.Listener.NoOp.INSTANCE;
    }
  }

  static class RedefinitionLoggingListener implements AgentBuilder.RedefinitionStrategy.Listener {

    private static final Logger log = LoggerFactory.getLogger(RedefinitionLoggingListener.class);

    @Override
    public void onBatch(final int index, final List<Class<?>> batch, final List<Class<?>> types) {}

    @Override
    public Iterable<? extends List<Class<?>>> onError(
        final int index,
        final List<Class<?>> batch,
        final Throwable throwable,
        final List<Class<?>> types) {
      if (DEBUG) {
        log.debug("Exception while retransforming {} classes: {}", batch.size(), batch, throwable);
      }
      return Collections.emptyList();
    }

    @Override
    public void onComplete(
        final int amount,
        final List<Class<?>> types,
        final Map<List<Class<?>>, Throwable> failures) {}
  }

  static class TransformLoggingListener implements AgentBuilder.Listener {

    private static final Logger log = LoggerFactory.getLogger(TransformLoggingListener.class);

    @Override
    public void onError(
        final String typeName,
        final ClassLoader classLoader,
        final JavaModule module,
        final boolean loaded,
        final Throwable throwable) {
      if (DEBUG) {
        log.debug(
            "Transformation failed - instrumentation.target.class={} instrumentation.target.classloader={}",
            typeName,
            classLoader,
            throwable);
      }
    }

    @Override
    public void onTransformation(
        final TypeDescription typeDescription,
        final ClassLoader classLoader,
        final JavaModule module,
        final boolean loaded,
        final DynamicType dynamicType) {
      if (DEBUG) {
        log.debug(
            "Transformed - instrumentation.target.class={} instrumentation.target.classloader={}",
            typeDescription.getName(),
            classLoader);
      }
    }

    @Override
    public void onIgnored(
        final TypeDescription typeDescription,
        final ClassLoader classLoader,
        final JavaModule module,
        final boolean loaded) {
      //      log.debug("onIgnored {}", typeDescription.getName());
    }

    @Override
    public void onComplete(
        final String typeName,
        final ClassLoader classLoader,
        final JavaModule module,
        final boolean loaded) {
      //      log.debug("onComplete {}", typeName);
    }

    @Override
    public void onDiscovery(
        final String typeName,
        final ClassLoader classLoader,
        final JavaModule module,
        final boolean loaded) {
      //      log.debug("onDiscovery {}", typeName);
    }
  }

  /**
   * Register a callback to run when a class is loading.
   *
   * <p>Caveats:
   *
   * <ul>
   *   <li>This callback will be invoked by a jvm class transformer.
   *   <li>Classes filtered out by {@link AgentInstaller}'s skip list will not be matched.
   * </ul>
   *
   * @param className name of the class to match against
   * @param callback runnable to invoke when class name matches
   */
  public static void registerClassLoadCallback(final String className, final Runnable callback) {
    if ("java.util.logging.LogManager".equals(className)) {
      LOG_MANAGER_CALLBACKS.add(callback);
    } else if ("javax.management.MBeanServerBuilder".equals(className)) {
      MBEAN_SERVER_BUILDER_CALLBACKS.add(callback);
    } else if (DEBUG) {
      log.debug("Callback not registered for unexpected class {}", className);
    }
  }

  private static class ClassLoadListener implements AgentBuilder.Listener {
    @Override
    public void onDiscovery(
        final String typeName,
        final ClassLoader classLoader,
        final JavaModule javaModule,
        final boolean b) {}

    @Override
    public void onTransformation(
        final TypeDescription typeDescription,
        final ClassLoader classLoader,
        final JavaModule javaModule,
        final boolean b,
        final DynamicType dynamicType) {}

    @Override
    public void onIgnored(
        final TypeDescription typeDescription,
        final ClassLoader classLoader,
        final JavaModule javaModule,
        final boolean b) {}

    @Override
    public void onError(
        final String s,
        final ClassLoader classLoader,
        final JavaModule javaModule,
        final boolean b,
        final Throwable throwable) {}

    @Override
    public void onComplete(
        final String typeName,
        final ClassLoader classLoader,
        final JavaModule javaModule,
        final boolean b) {
      final List<Runnable> callbacks;
      // On Java 17 java.util.logging.LogManager is loaded early even though it's not initialized,
      // so wait for the LoggerContext to be initialized before the callbacks are processed.
      if ("java.util.logging.LogManager$LoggerContext".equals(typeName)) {
        callbacks = LOG_MANAGER_CALLBACKS;
      } else if ("javax.management.MBeanServerBuilder".equals(typeName)) {
        callbacks = MBEAN_SERVER_BUILDER_CALLBACKS;
      } else {
        callbacks = null;
      }
      if (callbacks != null) {
        for (final Runnable callback : callbacks) {
          callback.run();
        }
      }
    }
  }

  private AgentInstaller() {}
}
```

### Utilsのソース

dd-java-agent/agent-tooling/src/main/java/datadog/trace/agent/tooling/Utils.java


```java
package datadog.trace.agent.tooling;

import datadog.trace.bootstrap.BootstrapProxy;
import java.lang.instrument.Instrumentation;
import java.util.concurrent.atomic.AtomicReference;

public class Utils {

  /** Return the classloader the core agent is running on. */
  public static ClassLoader getAgentClassLoader() {
    return Instrumenter.class.getClassLoader();
  }

  /** Return a classloader which can be used to look up bootstrap resources. */
  public static ClassLoader getBootstrapProxy() {
    return BootstrapProxy.INSTANCE;
  }

  private static final AtomicReference<Instrumentation> instrumentationRef =
      new AtomicReference<>();

  public static void setInstrumentation(Instrumentation instrumentation) {
    instrumentationRef.compareAndSet(null, instrumentation);
  }

  public static Instrumentation getInstrumentation() {
    return instrumentationRef.get();
  }

  private Utils() {}
}
```

### AgengBuilderの初期化箇所

```java
new AgentBuilder.Default(byteBuddy)
    .disableClassFormatChanges()
    .assureReadEdgeTo(inst, FieldBackedContextAccessor.class)
    .with(AgentStrategies.transformerDecorator()) // AgentStrategis::loadTransformerDecoratorが返す
      // 型はTransformerDecoratorで、実際のインスタンスは以下
      // net.bytebuddy.agent.builder.AgentBuilder.TransformerDecoratorは
      // エージェントがクラスファイルを変換する際に追加の変換を提供するために使用される。
      // TransformerDecoratorインターフェースには、decorateという一つのメソッドが定義されている。
      // このメソッドは、元のTransformerを引数に取り、新しいTransformerを返します。
      //  この新しいTransformerは、元のTransformerの変換を適用した後に追加の変換を適用します。
      //    jdk9以上の場合：datadog.trace.agent.tooling.bytebuddy.DDJava9ClassFileTransformer のインスタンス
      //    jdk8の場合: DDClassFileTransformerのインスタンス
      // transformメソッドでは、
      //    datadog.trace.agent.tooling.bytebuddy.matcher.ClassLoaderMatchers.canSkipClassLoaderByNameを呼び出して
      //    classLoaderの名前でtransformを呼び出す、呼び出さないを判断する。
      //    classLoaderが以下の場合はtransformを呼び出さない。
      //      - "org.codehaus.groovy.runtime.callsite.CallSiteClassLoader"
      //      - "sun.reflect.DelegatingClassLoader"
      //      - "jdk.internal.reflect.DelegatingClassLoader"
      //      - "clojure.lang.DynamicClassLoader"
      //      - "org.apache.cxf.common.util.ASMHelper$TypeHelperClassLoader"
      //      - "sun.misc.Launcher$ExtClassLoader"
      //      - "datadog.trace.bootstrap.DatadogClassLoader"
      //  また、終了後にSharedTypePools.endTransform()を呼び出す
    .with(AgentBuilder.RedefinitionStrategy.RETRANSFORMATION) // 再変換できるようにする
    .with(AgentStrategies.rediscoveryStrategy())
    // 変換された場合に通知を受けるか？
    //   IASTが有効な場合はTaintableRedefinitionStrategyListenerを呼び出してもらう？
    //   そうではない場合は、特に何もしない
    .with(redefinitionStrategyListener(enabledSystems)) //
    .with(AgentStrategies.locationStrategy())
    // ClassFileLocatorsでクラスファイルの場所を返却するように設定する
    // ClassFileLocatorsは、Utils.getBootstrapProxy()を使ってURLを取得する。
    // BootstrapProxyは、

    .with(AgentStrategies.poolStrategy())
    //DDOutlinePoolStrategyのインスタンス( AgentBuilder.PoolStrategy)として指定する
    // PoolStrategy.typePool()はswitchContextしたTypePollFacadインスタンスを返す
    // ※1/19 TypePoolについておさらいする。

    .with(AgentBuilder.DescriptionStrategy.Default.POOL_ONLY)
    //Javaエージェントは、クラスを変換する前にそのクラスのメタデータ（クラス名、メソッド名、フィールド名など）を取得する
    //DescriptionStrategy.Default.POOL_ONLYを指定すると、
    // クラスのメタデータを取得するために、クラスファイルのバイトコードを解析するタイププールだけを使用します。
    // これは、クラスがまだロードされていない場合や、クラスのバイトコードがクラスローダーから直接アクセスできない場合に有用です。
    
    .with(AgentStrategies.bufferStrategy())
    // DDOutlineTypeStrategyのインスタンスをClassFileBufferStrategyとして適用
    
    .with(AgentStrategies.typeStrategy())
    // DDOutlineTypeStrategyのTypeStrategyインタフェースとして適用

    .with(new ClassLoadListener())
    // - java.util.logging.LogManager$LoggerContextが変換された場合
    //    - CopyOnWriteArrayListを実行する
    // - javax.management.MBeanServerBuilderが変換された場合
    //    - CopyOnWriteArrayListを実行する

    
    // FIXME: we cannot enable it yet due to BB/JVM bug, see
    // https://github.com/raphw/byte-buddy/issues/558
    // .with(AgentBuilder.LambdaInstrumentationStrategy.ENABLED)
    // LambdaInstrumentationStrategyが正規表現を処理しない

    .ignore(globalIgnoresMatcher(skipAdditionalLibraryMatcher));
```

## 参考

### net.bytebuddy.agent.builder.AgentBuilder.TransformerDecorator

net.bytebuddy.agent.builder.AgentBuilder.TransformerDecoratorは、Byte Buddyライブラリの一部で、Javaエージェントの作成に使用されます。
このインターフェースは、エージェントがクラスファイルを変換する際に追加の変換を提供するために使用されます。

TransformerDecoratorインターフェースには、decorateという一つのメソッドが定義されています。
このメソッドは、元のTransformerを引数に取り、新しいTransformerを返します。この新しいTransformerは、元のTransformerの変換を適用した後に追加の変換を適用します。

以下に、TransformerDecoratorの使用例を示します。

この例では、TransformerDecoratorは元のTransformerの変換を適用した後に、すべてのtoStringメソッドを"transformed"という文字列を返すように変換します。

```java{.line-numbers}
AgentBuilder agentBuilder = new AgentBuilder.Default()
    .with(new AgentBuilder.InitializationStrategy.SelfInjection.Eager())
    .with(new AgentBuilder.RedefinitionStrategy.DiscoveryStrategy.Reiterating())
    .with(new AgentBuilder.TypeStrategy.Default())
    .with(new AgentBuilder.Listener.WithErrorsOnly())
    .with(new AgentBuilder.Listener.WithTransformationsOnly())
    .with(new AgentBuilder.TransformerDecorator() {
        @Override
        public AgentBuilder.Transformer decorate(AgentBuilder.Transformer transformer) {
            return new AgentBuilder.Transformer() {
                @Override
                public DynamicType.Builder<?> transform(DynamicType.Builder<?> builder,
                                                        TypeDescription typeDescription,
                                                        ClassLoader classLoader,
                                                        JavaModule module) {
                    // Apply the original transformation
                    builder = transformer.transform(builder, typeDescription, classLoader, module);
                    // Apply additional transformation
                    return builder.method(named("toString")).intercept(FixedValue.value("transformed"));
                }
            };
        }
    });
```


